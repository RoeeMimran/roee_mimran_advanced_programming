#pragma once

#include <iostream>

using namespace std;

typedef struct listNode
{
	unsigned int num;
	listNode* next;
}listNode;

void addNode(listNode** head, listNode* newNode);

void removeNode(listNode** head);