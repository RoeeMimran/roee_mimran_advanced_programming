#include "stack.h"

/*
function put node with number at the head
input: the head of the list and the wanted number
output: none
*/
void push(stack* s, unsigned int element)
{
	listNode* newNode = new listNode;
	newNode->num = element;

	addNode(&(s->list), newNode);
}

/*
function remove the top of the list
input: the head of the list
output: the first number or -1 if the stack empty
*/
int pop(stack* s)
{
	int numToDelete = LIST_EMPTY;

	if (s->list) // checking if the list empty
	{
		numToDelete = s->list->num;
		removeNode(&(s->list));
	}

	return numToDelete;
}

/*
function init the stack, create dynamic memory for the node
input: the stack to init
output: none
*/
void initStack(stack* s)
{
	listNode* sList = new listNode;

	sList->next = 0;
	sList->num = 0;
	s->list = sList;
}

/*
function delete all the dynamic memory
input: the the head of the list
output: none
*/
void cleanStack(stack* s)
{
	listNode* temp = 0;
	while (s->list)
	{
		temp = s->list; // save the current node

		s->list = s->list->next; // update the list

		temp->next = 0; // reset the variables
		temp->num = 0;

		delete temp; // delete current node
	}
	delete s;
}