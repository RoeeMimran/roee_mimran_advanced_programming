#include "LinkedList.h"

/*
function add node to the list
input: the head and the new node
output: none
*/
void addNode(listNode** head, listNode* newNode)
{
	newNode->next = *head;
	*head = newNode;
}

/*
function remove node from the list
input: the head of the list
output: none
*/
void removeNode(listNode** head)
{
	listNode* temp = *head;
	*head = (*head)->next;

	delete temp;
}