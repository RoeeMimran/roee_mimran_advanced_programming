#include "stack.h"
#include "utlis.h"

/*
function reverse the array of numbers
input: the array of numbers and his size
output: none
*/
void reverse(int* nums, unsigned int size)
{
	stack* head = arrayToStack(nums, size); 
	stackToArray(head, size, nums); 

	cleanStack(head);
}

/*
function convert stack to array of numbers
input: the stack, the size of the array, the array to put the numbers
output: none
*/
void stackToArray(stack* head, unsigned int size, int* array)
{
	int i = 0;

	for (i = 0; i < size; i++)
	{
		*(array + i) = pop(head); // pop wvery number to the array
	}
}

/*
function converts array of numbers to stack
input: the array of numbers and his size
output: the head of the stack
*/
stack* arrayToStack(int* array, unsigned int size)
{
	int i = 0;
	stack* head = new stack;
	
	initStack(head); // initallize the list

	for (i = 0; i < size; i++)
	{
		push(head, *(array + i)); // push every number
	}
	return head;
}

/*
function collect 10 numbers and reverse it 
input: none
output: the array of number
*/
int* reverse10()
{
	int i = 0;
	int* nums = new int[MAX_NUMS];

	for (i = 0; i < MAX_NUMS; i++) // collect number
	{
		cout << i + 1 << ": ";
		cin >> *(nums + i);
	}

	reverse(nums, MAX_NUMS);

	return nums;
}