#include "queue.h"
using namespace std;

/*
function change the size of the array
input: the old array and his size and the size of the new array, and the index we want to start from
output: the new array
*/
unsigned int* changeArraySize(unsigned int* oldArray, int oldSize, int newSize, int startIndex)
{
	int i = 0;
	unsigned int* newArray = new unsigned int[newSize];

	for (i = 0; i < newSize; i++) // copy each number
	{
		*(newArray + i) = *(oldArray + startIndex + i);
	}

	delete oldArray;

	return newArray;
}

/*
function init the first node in the queue
input: the node the init and his max size
*/
void initQueue(queue* q, unsigned int size)
{
	q->maxSize = size;
	q->currSize = 0;
	q->nums = new unsigned int[0];
}

/*
function delete all dynamic memory and reset the other variables
input: the head of the queue
output: none
*/
void cleanQueue(queue* q)
{
	int i = 0;

	for (i = 0; i < q->currSize; i++) // reset all the numbers in the array
	{
		*(q->nums + i) = 0;
	}
	q->currSize = 0;
	q->maxSize = 0;

	delete q->nums;
	delete q;
}

/*
function add number at the head of the queue
input: the queue and the new value to put
output: noen
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->currSize + 1 <= q->maxSize)
	{
		q->nums = changeArraySize(q->nums, q->currSize, q->currSize + 1, 0); // change the array size to the new value

		*(q->nums + q->currSize) = newValue;
		(q->currSize)++; // increase the size by one to the new value
	}
	else
	{
		cout << "not enough space for new value" << endl;
	}
}

/*
function remove the first number from the queue
input: the queue with the numbers
output: the number that removed or -1 if the queue empty
*/
int dequeue(queue* q)
{
	int firstNum = 0;

	if (q->currSize > 0) // checking for first number
	{
		firstNum = *(q->nums); // save the first number

		q->nums = changeArraySize(q->nums, q->currSize, q->currSize - 1, 1); // resize the array from the first number
		(q->currSize)--; // decreased by 1 the size
	}
	else
	{
		firstNum = LIST_EMPTY;
	}

	return firstNum;
}