#pragma once

#include <iostream>
using namespace std;

#ifndef QUEUE_H
#define QUEUE_H

#define LIST_EMPTY -1

/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* nums;
	int maxSize;
	int currSize;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */