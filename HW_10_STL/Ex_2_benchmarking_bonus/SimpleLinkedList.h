#pragma once
#include "Facade.h"
#include <list>

template <class T>
class SimpleLinkedList : public Facade<T>
{
public:
	virtual bool add(const T item)
	{
		try
		{
			this->_myLinkedList.push_back(item);
			return true;
		}
		catch (...)
		{
			return false;
		}
	}

	virtual bool contains(const T item)
	{
		return std::find(this->_myLinkedList.begin(), this->_myLinkedList.end(), item) != this->_myLinkedList.end();
	}

	std::list<T> _myLinkedList;
};