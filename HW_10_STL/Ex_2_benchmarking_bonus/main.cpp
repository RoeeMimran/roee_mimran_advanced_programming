#include "SimpleDeque.h"
#include "SimpleLinkedList.h"
#include "SimpleHashMap.h"
#include "SimpleTreeSet.h"
#include "SimpleVector.h"
#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

#define DATA1_FILE_NAME "data1.txt"
#define DATA2_FILE_NAME "data2.txt"
#define CANT_OPEN_FILE_ERROR "Unable to open file file.txt"

#define DATA1_STRING_TO_FIND1 "hi"
#define DATA1_STRING_TO_FIND2 "-13170890158"

#define DATA2_STRING_TO_FIND1 "hi"
#define DATA2_STRING_TO_FIND2 "23"

/*
function add the elements to the container
input: the container and the file to read from
output: none
*/
template <class T>
void addingElements(T& container, std::ifstream& file)
{
	std::string line;

	auto start = std::chrono::high_resolution_clock::now();

	// add every line to the container
	while (std::getline(file, line))
	{
		container.add(line);
	}

	// print the time it took
	auto finish = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = finish - start;
	std::cout << "time: " << elapsed.count() << std::endl;;

	// rewind the file
	file.clear();
	file.seekg(0, std::ios::beg);
}


/*
function search an element in the container
input: the container to search in and the element ot look for
output: none
*/
template <class T>
void searchElements(T& container, std::string element)
{
	std::string line;

	auto start = std::chrono::high_resolution_clock::now();

	container.contains(element);
	
	auto finish = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = finish - start;
	std::cout << "time: " << elapsed.count() << std::endl;;
}

/*
function test the container 
input: the file to read from the strings for testing, and two strings to look for in the container
output: none
*/
void testContainers(std::string file_name, std::string stringsToLookFor[])
{
	SimpleDeque<std::string> myDeque;
	SimpleLinkedList<std::string> myList;
	SimpleHashMap<std::string> myMap;
	SimpleTreeSet<std::string> mySet;
	SimpleVector<std::string> myVector;

	std::ifstream file(file_name);

	// checking if the file exist
	if (!file)
	{
		std::cerr << CANT_OPEN_FILE_ERROR << std::endl;
		system("PAUSE");
		exit(1);
	}

	std::cout << "adding elements: " << std::endl;
	addingElements(myDeque, file);
	addingElements(myList, file);
	addingElements(myMap, file);
	addingElements(mySet, file);
	addingElements(myVector, file);

	std::cout << std::endl << std::endl << "searching - " << stringsToLookFor[0] << ":" << std::endl;
	searchElements(myDeque, stringsToLookFor[0]);
	searchElements(myList, stringsToLookFor[0]);
	searchElements(myMap, stringsToLookFor[0]);
	searchElements(mySet, stringsToLookFor[0]);
	searchElements(myVector, stringsToLookFor[0]);

	std::cout << std::endl << std::endl << "searching - " << stringsToLookFor[1] << ":" << std::endl;
	searchElements(myDeque, stringsToLookFor[1]);
	searchElements(myList, stringsToLookFor[1]);
	searchElements(myMap, stringsToLookFor[1]);
	searchElements(mySet, stringsToLookFor[1]);
	searchElements(myVector, stringsToLookFor[1]);

	file.close();
}


int main()
{
	std::cout << "data1: " << std::endl;
	std::string firstFileStrings[2] = { DATA1_STRING_TO_FIND1, DATA1_STRING_TO_FIND2 };
	testContainers(DATA1_FILE_NAME, firstFileStrings);

	std::cout << std::endl << std::endl << "data2: " << std::endl;
	std::string secondFileStrings[2] = { DATA2_STRING_TO_FIND1, DATA2_STRING_TO_FIND2 };
	testContainers(DATA2_FILE_NAME, secondFileStrings);
	system("PAUSE");
	return 0;
}
