#pragma once
#include "Facade.h"
#include <set>

template <class T>
class SimpleTreeSet : public Facade<T>
{
public:
	virtual bool add(const T item)
	{
		try
		{
			this->_mySet.insert(item);
			return true;
		}
		catch (...)
		{
			return false;
		}

		
	}

	virtual bool contains(const T item)
	{
		return std::find(this->_mySet.begin(), this->_mySet.end(), item) != this->_mySet.end();
	}

	std::set<T> _mySet;
};
