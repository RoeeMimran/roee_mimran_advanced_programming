#pragma once

template <class T>
class Facade
{
protected:
	virtual bool add(const T item) = 0;
	virtual bool contains(const T item) = 0;
};
