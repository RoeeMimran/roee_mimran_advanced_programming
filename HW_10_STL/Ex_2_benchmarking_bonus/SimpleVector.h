#pragma once
#include "Facade.h"
#include <vector>

template <class T>
class SimpleVector : public Facade<T>
{
public:
	virtual bool add(const T item)
	{
		try
		{
			this->_myVector.push_back(item);
			return true;
		}
		catch (...)
		{
			return false;
		}
		
	}

	virtual bool contains(const T item)
	{
		return std::find(this->_myVector.begin(), this->_myVector.end(), item) != this->_myVector.end();
	}

	std::vector<T> _myVector;
};
