#pragma once
#include "Facade.h"
#include <unordered_set>

template <class T>
class SimpleHashMap : public Facade<T>
{
public:
	virtual bool add(const T item)
	{
		try
		{
			this->_myMap.insert(item);
			return true;
		}
		catch (...)
		{
			return false;
		}
	}

	virtual bool contains(const T item)
	{
		return std::find(this->_myMap.begin(), this->_myMap.end(), item) != this->_myMap.end();
	}

	std::unordered_set<T> _myMap;
};
