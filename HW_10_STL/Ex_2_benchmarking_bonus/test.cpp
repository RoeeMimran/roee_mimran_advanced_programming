#include "SimpleDeque.h"
#include "SimpleLinkedList.h"
#include "SimpleHashMap.h"
#include "SimpleTreeSet.h"
#include "SimpleVector.h"
#include <iostream>


template <class T>
void test(T& a)
{
	std::cout << a.add(3) << std::endl;
	std::cout << a.contains(2) << std::endl << std::endl;
}

void testAll()
{
	SimpleDeque<int> a;
	std::cout << "deque:" << std::endl;
	test(a);

	SimpleLinkedList<int> b;
	std::cout << "linked_list:" << std::endl;
	test(b);

	SimpleHashMap<int> c;
	std::cout << "hash map:" << std::endl;
	test(c);

	SimpleTreeSet<int> d;
	std::cout << "set:" << std::endl;
	test(d);

	SimpleVector<int> e;
	std::cout << "vector:" << std::endl;
	test(e);
}
