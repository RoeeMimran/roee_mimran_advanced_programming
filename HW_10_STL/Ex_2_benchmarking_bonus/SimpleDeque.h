#pragma once
#include "Facade.h"
#include <deque>

template <class T>
class SimpleDeque : public Facade<T>
{
public:
	virtual bool add(const T item)
	{
		try
		{
			this->_myDeque.push_back(item);
			return true;
		}
		catch (...)
		{
			return false;
		}
		
	}

	virtual bool contains(const T item)
	{
		return std::find(this->_myDeque.begin(), this->_myDeque.end(), item) != this->_myDeque.end();
	}

	std::deque<T> _myDeque;
};