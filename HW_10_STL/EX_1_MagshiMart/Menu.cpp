#include "Menu.h"

/*
c'tor
function initialized the menu
input: the map of the customers and the array of the item list and his size
output: none
*/
Menu::Menu(std::map<std::string, Customer>* abcCustomers, Item* itemList, unsigned int itemListSize)
{
	this->_abcCustomers = abcCustomers;
	this->_itemList = itemList;
	this->_itemListSize = itemListSize;
}

/*
function get the choice of the user and choose what function to call
input: none
output: none
*/
void Menu::martProg()
{
	int choice = 0;

	while (choice != EXIT)
	{
		choice = this->mainMenu();
		std::cout << std::endl;

		switch (choice)
		{
		case SIGN_UP_AND_BUY:
			this->signAndBuy();
			break;

		case UPDATE_AND_BUY:
			this->updateAndBuy();
			break;

		case PRINT_THE_MOST_PAY:
			this->printeTheMostPay();
			break;
		
		case EXIT:
			break;

		default:
			std::cout << NOT_EXIST_OPTION << std::endl;
			break;
		}
		system("PAUSE"); // wait for user to press enter
		system("CLS"); // clear the screen
	}
}

/*
function print the main menu
input: none
output: the choice of the user
*/
int Menu::mainMenu()
{
	int choice = 0;

	std::cout << "Welcome to MagshiMart!" << std::endl;
	std::cout << "1. to sign as customer and buy items" << std::endl;
	std::cout << "2. to uptade existing customer's items" << std::endl;
	std::cout << "3. to print the customer who pays the most" << std::endl;
	std::cout << "4. to exit" << std::endl;
		
	std::cin >> choice;
	return choice;
}


/*
function print the menu of the second option, the update and buy
input: none
output: the choice of the user
*/
int Menu::updateMenu()
{
	int choice = 0;

	std::cout << "1. Add items" << std::endl;
	std::cout << "2. Remove items" << std::endl;
	std::cout << "3. Back to menu" << std::endl;

	std::cin >> choice;
	return choice;
}

/*
function sign the user in the system, check if the user is already exist
if not, he can buy an item
input: none
output: none
*/
void Menu::signAndBuy()
{
	std::string newName = "";
	std::cout << "enter the new name: ";
	std::cin >> newName;

	// checking if the user exist
	if (this->_abcCustomers->find(newName) == this->_abcCustomers->end())
	{
		// add the user to the map
		(*this->_abcCustomers)[newName] = Customer(newName);
		this->buy(newName);
	}
	else
	{
		std::cout << USER_EXIST_ERROR << std::endl;
	}
}

/*
function let the user choose an item and buy it
input: the name of the buyer
output: none
*/
void Menu::buy(const std::string buyerName)
{
	unsigned int wantedItem = this->chooseItem();

	// checking if the wanted item is valid option
	if (wantedItem < this->_itemListSize)
	{
		(*this->_abcCustomers)[buyerName].addItem(this->_itemList[wantedItem]);
	}
	else
	{
		std::cout << NOT_EXIST_OPTION << std::endl;
	}
	
}

/*
function is the second option in the menu, update the user
input: none
output: none
*/
void Menu::updateAndBuy()
{
	std::string name = "";
	std::cout << "enter the name: ";
	std::cin >> name;

	// checking if the user doesn't exist
	if (this->_abcCustomers->find(name) == this->_abcCustomers->end())
	{
		std::cout << USER_DOESNT_EXIST_ERROR << std::endl;
	}
	else
	{
		this->printMyItems(name);
		int choice = this->updateMenu();

		switch (choice)
		{
		case BUY_ITEMS:
			this->buy(name);
			break;

		case REMOVE_ITEM:
			this->removeItem(name);
			break;

		case BACK_TO_MENU:
			break;

		default:
			std::cout << NOT_EXIST_OPTION << std::endl;
			break;
		}

		// print his total sum at the end of the update
		std::cout << "your total sum: " << (*this->_abcCustomers)[name].totalSum() << std::endl;
	}
}

/*
function remove an item from his items
input: the name of the customer
output: none
*/
void Menu::removeItem(const std::string buyerName)
{
	// get the customer
	Customer& customer = (*this->_abcCustomers)[buyerName];

	// get his choice, his wanted item
	unsigned int wantedItem = 0;
	std::cout << "choose one of the items above" << std::endl;
	std::cin >> wantedItem;

	// dec 1 because we start printing from 1
	wantedItem--;
	
	std::set<Item> temp = customer.getItems();

	// checking if the wanted item is valid option
	if (wantedItem < temp.size())
	{
		// remove the item from the list
		customer.removeItem(*std::next(temp.begin(), wantedItem));
	}
	else
	{
		std::cout << NOT_EXIST_OPTION << std::endl;
	}	
}

/*
function print the most pay customer
input: none
output: none
*/
void Menu::printeTheMostPay()
{
	std::map<std::string, Customer>::iterator it = this->_abcCustomers->begin();
	Customer* paysTheMost = &it->second;
	++it;

	// checking who has the biggest total sum
	for (it; it != this->_abcCustomers->end(); ++it)
	{
		if (paysTheMost->totalSum() < it->second.totalSum())
		{
			paysTheMost = &it->second;
		}
	}

	std::cout << "the customer who pays the most: " << paysTheMost->getName() << std::endl;
}

/*
function print all the items and let the user choose an item
input: none
output: his choice
*/
unsigned int Menu::chooseItem() const
{
	unsigned int choice = 0;
	
	std::cout << std::endl << "items:" << std::endl;

	// print all the items from number 1
	for (int i = 0; i < this->_itemListSize; i++)
	{
		std::cout << i + 1 << '.' << this->_itemList[i].getName();
		std::cout << "   price: " << this->_itemList[i].totalPrice();
		std::cout << std::endl;
	}

	std::cout << "your choice: ";
	std::cin >> choice;
	return choice - 1; // dec 1 because we printed from number 1
}

/*
function print my items for the second optin of the main menu
input: the customer name
output: none
*/
void Menu::printMyItems(std::string customerName) const 
{
	std::set<Item>::iterator it;
	int i = 0;

	// get the customer
	Customer& customer = (*this->_abcCustomers)[customerName];
	std::set<Item> temp = customer.getItems();

	// print his items and the count
	std::cout << std::endl << "your items: " << std::endl;
	for (std::set<Item>::iterator it = temp.begin(); it != temp.end(); ++it)
	{
		i++;
		std::cout << i << ". " << it->getName() << ", count: " << it->getCount() << std::endl;
	}
	std::cout << std::endl;
}