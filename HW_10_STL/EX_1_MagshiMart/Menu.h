#include <iostream>
#include "Item.h"
#include "Customer.h"
#include <map>

#define USER_EXIST_ERROR "this name is already exist"
#define USER_DOESNT_EXIST_ERROR "this name doesn't exist, you need to sign in first"
#define NOT_EXIST_OPTION "choose one of the options above"

enum mainChoices
{
	SIGN_UP_AND_BUY = 1,
	UPDATE_AND_BUY,
	PRINT_THE_MOST_PAY,
	EXIT
};

enum updateChoices
{
	BUY_ITEMS = 1,
	REMOVE_ITEM,
	BACK_TO_MENU
};

class Menu
{
public:
	Menu(std::map<std::string, Customer>* abcCustomers, Item* itemList, unsigned int itemListSize);
	void martProg();

private:
	// menus
	int mainMenu();
	int updateMenu();

	// main options
	void signAndBuy();
	void updateAndBuy();
	void printeTheMostPay();

	// helping funtion
	void buy(const std::string buyerName);
	void removeItem(const std::string buyerName);
	unsigned int chooseItem() const ;
	void printMyItems(std::string customerName) const;

	// fields
	std::map<std::string, Customer>* _abcCustomers;
	Item* _itemList;
	unsigned int _itemListSize;
};