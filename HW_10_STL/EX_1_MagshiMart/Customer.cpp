#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::Customer()
{
}

/*
function calculate the total sum
input: none
output: the total sum of thre customer
*/
double Customer::totalSum() const
{
	double sum = 0;
	std::set<Item>::iterator it;

	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		sum += it->totalPrice();
	}

	return sum;
}

/*
funciton add an item to the set, if added already: add 1 to count
input: the item to add
output: none
*/
void Customer::addItem(Item item)
{
	std::set<Item>::iterator it = this->_items.find(item);

	if (it != this->_items.end())
	{
		// item is already exist
		item.setCount(it->getCount()); // copy his count
		item.operator++(); // add 1 to his count
		this->_items.erase(item);	// remove the old one
	}

	this->_items.insert(item);
}


/*
function remove the item from the set
input: the item to remove
output: none
*/
void Customer::removeItem(Item item)
{
	std::set<Item>::iterator it = this->_items.find(item);

	// checking if the item exist and erase it
	if (it != this->_items.end())
	{
		// dec the count by 1
		Item temp = *it;
		temp.operator--();

		// remove it from the set
		this->_items.erase(it);

		// check if there are still items
		if (temp.getCount() > 0)
		{
			// add them to the set
			this->_items.insert(temp);
		}
	}
}

std::string Customer::getName() const
{
	return this->_name;
}

std::set<Item> Customer::getItems() const
{
	return this->_items;
}