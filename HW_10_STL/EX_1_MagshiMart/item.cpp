#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

bool Item::operator<(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator>(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;
}

std::string Item::getName() const
{
	return this->_name;
}

int Item::getCount() const
{
	return this->_count;
}

void Item::setCount(int count)
{
	this->_count = count;
}

void Item::operator++()
{
	this->_count++;
}

void Item::operator--()
{
	this->_count--;
}
