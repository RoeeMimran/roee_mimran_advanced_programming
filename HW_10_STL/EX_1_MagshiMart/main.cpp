#include"Customer.h"
#include<map>
#include "Menu.h"

#define ITEM_LIST_SIZE 10

int main()
{
	std::map<std::string, Customer> abcCustomers;
	Item itemList[ITEM_LIST_SIZE] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };
		
	Menu menu(&abcCustomers, itemList, ITEM_LIST_SIZE);
	menu.martProg();
	
	return 0;
}