#include "threads.h"

void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread caller(I_Love_Threads);
	caller.join();
}

/*
function print the prime numbers
input: the vector with the prime numebers
output: none
*/
void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
	{
		std::cout << primes[i] << std::endl;
	}
}

/*
function get the prime numbers according to the parameters
input: the begining of the numebrs and to the end, the vector to push all the prime numebrs
output: none
*/
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int i = 0;
	int j = 0;
	bool isPrime = true;
	
	for (i = begin; i <= end; i++)
	{
		isPrime = true;
		
		// checking if the numebr can be devided by one of the numbers below
		for (j = i - 1; j > 1; j--)
		{
			if (i % j == 0)
			{
				isPrime = false;
				break;
			}
		}

		if (isPrime)
		{
			primes.push_back(i);
		}
	}
}

/*
function call a thread to get the primes
input: the begining of the prime numebrs to the end
output: the vecor with the prime numebrs
*/
std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primeNumbers;

	Clock clock; // start measure the time
	std::thread caller(getPrimes, begin, end, std::ref(primeNumbers));
	caller.join();
	
	printVector(primeNumbers);
	return primeNumbers;
}

/*
function get the prime numbers between the limits and write it to the file
input: being, end - the limits of the prime numbers
file - the file to write the prime numbers
output: none
*/
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	std::vector<int> primes = callGetPrimes(begin, end);

	// write the prime numbers to the file
	for (int i = 0; i < primes.size(); i++)
	{
		file << primes[i] << std::endl;
	}
}

/*
function devide the limits to number of threads and get  the prime numebers between the lmits
input: begin, end - the limits of the prime numbers
filepath - the path of the file to write the prime numbers
N - number of threads
output: none
*/
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	ThreadsHandle handle;
	std::ofstream primesFile(filePath);
	
	// get the numbers to check for each thread
	int numbersToCheck = (end - begin) / N;

	Clock clock; // start measureing the the imte

	// add each thread with his params
	for (int i = 0; i < N; i++)
	{
		int currBegin = i * numbersToCheck + begin;
		int currEnd = currBegin + numbersToCheck;
		
		handle.addThread(currBegin, currEnd, primesFile);
	}

	// start all the threads
	handle.start();
	std::cout << "time for whole threads: ";
}
