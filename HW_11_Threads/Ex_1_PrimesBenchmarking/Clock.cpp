#include "Clock.h"

/*
c'tor
function start measure the time
input: none
output: none
*/
Clock::Clock()
{
	this->_begin = GetTickCount64();
}

/*
d'tor
function finish measure the time and print the difference
input: none
output: none
*/
Clock::~Clock()
{
	DWORD end = GetTickCount64();
	std::cout << end - this->_begin << " ms" << std::endl;
}
