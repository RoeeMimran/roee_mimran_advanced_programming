#include "ClientManager.h"

// c'tor
ClientManager::ClientManager(std::mutex& msgMtx, std::queue<Message>& messages, SOCKET clientSocket, std::string clientName)
	: _msgMtx(msgMtx), _messages(messages)
{
	this->_clientSocket = clientSocket;
	this->_clientName = clientName;
}

/*
function receive messages for the client and upload the message to the global queue
input: the mutex of the messages to not interrupt other threads and the cv to wake the response thread
output: none
*/
void ClientManager::receiveMessages(std::mutex& msgMtx, std::condition_variable& cv)
{
	int usernameLen = 0;
	std::string username;
	int msgLen = 0;
	std::string msg;

	while (true)
	{
		try
		{
			Helper::getMessageTypeCode(this->_clientSocket);
			usernameLen = Helper::getIntPartFromSocket(this->_clientSocket, USERNAME_LEN_BYTES);
			username = Helper::getStringPartFromSocket(this->_clientSocket, usernameLen);
			msgLen = Helper::getIntPartFromSocket(this->_clientSocket, MSG_LEN_BYTES);
			msg = Helper::getStringPartFromSocket(this->_clientSocket, msgLen);			
		}
		catch (...)
		{
			// create a quit message and break the loop
			std::unique_lock<std::mutex> locker(msgMtx);
			
			this->_messages.push(Message(this->_clientName, QUIT, QUIT));

			cv.notify_one();
			break;
		}

		std::unique_lock<std::mutex> locker(msgMtx);	

		// if the message is empty so we need to return it to the sender
		if (!msg.empty())
		{
			this->_messages.push(Message(this->_clientName, username, msg));
		}
		else
		{
			this->_messages.push(Message(username, this->_clientName, msg));
		}
		
		// wake the respone thread
		locker.unlock();
		cv.notify_one();
	}
}