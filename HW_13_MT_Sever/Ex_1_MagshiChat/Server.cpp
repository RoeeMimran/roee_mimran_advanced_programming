#include "Server.h"
#include <exception>
#include <iostream>
#include <string>

Server::Server(std::map<std::string, SOCKET>& users, std::mutex& usersMtx, std::condition_variable& usersCV, std::queue<std::string> &newUsers) 
	: _users(users), _userMtx(usersMtx), _usersCV(usersCV), _newUsers(newUsers)
{
	std::cout << "starting..." << std::endl;

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	try
	{
		if (_serverSocket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__ " - socket");
	}
	catch (const std::exception & e)
	{
		std::cout << e.what() << std::endl;
		system("PAUSE");
		_exit(1);
	}
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	try
	{
		// again stepping out to the global namespace
		// Connects between the socket and the configuration (port and etc..)
		if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
			throw std::exception(__FUNCTION__ " - bind");
		std::cout << "binded" << std::endl;

		// Start listening for incoming requests of clients
		if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
			throw std::exception(__FUNCTION__ " - listen");
		std::cout << "Listening..." << std::endl;
	}
	catch (const std::exception & e)
	{
		std::cout << e.what() << std::endl;
		system("PAUSE");
		_exit(1);
	}

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "accpeting client..." << std::endl;
		accept();
	}
}

void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted!" << std::endl;
	std::thread newClient(&Server::login, this, client_socket);
	newClient.detach();
}

/*
function add the user to the map and his socket, send to the server a login message
input: the socket of the client
output: none
*/
void Server::login(SOCKET clientSocket)
{
	std::string username;
	int usernameLen = 0;
	try
	{
		// get the len of the user name
		Helper::getMessageTypeCode(clientSocket);
		usernameLen = Helper::getIntPartFromSocket(clientSocket, SIZE_OF_LEN);

		// get his name
		username = Helper::getStringPartFromSocket(clientSocket, usernameLen);

		// add him to user and wake the clients
		std::unique_lock<std::mutex> locker(this->_userMtx);

		this->_users[username] = clientSocket;
		this->_newUsers.push(username);

		std::cout << "ADDED new client " << clientSocket << ", " << username << " to clients list" << std::endl;

		this->sendLoginMSg();
		this->_usersCV.notify_one();
	}
	catch (...)
	{
		this->_users.erase(username);
	}
}

/*
function send the each user the login msg in order to update the user list in the GUI
input: void
output: void
*/
void Server::sendLoginMSg()
{
	// get all the user names with & between each
	std::string allUsers = "";
	for (auto it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		allUsers = allUsers + it->first + SEPARATOR;
	}
	allUsers.erase(allUsers.size() - 1); // remove the last &

	// update each client that we have a new client
	for (auto it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		Helper::send_update_message_to_client(it->second, "", "", allUsers);
	}
}
