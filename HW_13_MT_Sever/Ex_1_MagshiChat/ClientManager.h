#pragma once

#include "Server.h"
#include <queue>
#include "Message.h"

#define USERNAME_LEN_BYTES 2
#define MSG_LEN_BYTES 5
#define QUIT "q"

class ClientManager
{
private:
	std::string _clientName;
	SOCKET _clientSocket;
	std::mutex& _msgMtx;
	std::queue<Message>& _messages;

	void createMessage(std::string sender, std::string receiver, std::string content);

public:
	ClientManager(std::mutex& msgMtx, std::queue<Message>& messages, SOCKET clientSocket, std::string clientName);
	void receiveMessages(std::mutex& msgMtx, std::condition_variable& cv);
};

