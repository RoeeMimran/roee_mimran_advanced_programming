#include "Response.h"

Response::Response(std::queue<Message>& messages, std::map<std::string, SOCKET>& users)
	: _messages(messages), _users(users)
{
}

/*
function send the messages from the queue to the clients
input: the mutexs and the condition variable to synchronize the users
output: none
*/
void Response::messagesSender(std::mutex& msgMtx, std::mutex& userMtx, std::condition_variable& cv)
{
	while (true)
	{	
		// wait for new messages
		std::unique_lock<std::mutex> locker(msgMtx);
		cv.wait(locker, [this]() {return !this->isMessagesEmpty(); });
		
		while (!this->isMessagesEmpty())
		{
			// checking if it is quit message
			if (this->_messages.front()._content == QUIT && this->_messages.front()._receiver == QUIT)
			{
				this->disconnected(this->_messages.front());
			}
			else
			{
				std::string filePath = this->saveChat(this->_messages.front());
				this->sendNewMsg(filePath, this->_messages.front());
			}
		
			this->_messages.pop();
			
		}
		locker.unlock();
	}
}

/*
function get the user in 1 string that seperate it &
input: none
output: the string with the users
*/
std::string Response::getUsers()
{
	// get all the user names with & between each
	std::string allUsers = "";
	for (auto it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		allUsers = allUsers + it->first + SEPARATOR;
	}
	allUsers.erase(allUsers.size() - 1); // remove the last &

	return allUsers;
}

/*
function send all the users to all the users
input: none
output: none
*/
void Response::sendAllUsers()
{
	std::string allUsers = this->getUsers();

	// update each client that we have a new client
	for (auto it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		Helper::send_update_message_to_client(it->second, "", "", allUsers);
	}
}

// getter for messages, to check for empty
bool Response::isMessagesEmpty()
{
	return this->_messages.empty();
}

/*
function save the chat, add to the file the new message 
input: the message to add
output: the path of the file
*/
std::string Response::saveChat(const Message msg)
{
	std::fstream file;
	std::string path;

	// checking which way to create the file
	if (msg._sender < msg._receiver)
	{
		path = msg._receiver + SEPARATOR + msg._sender + ".txt";
	}
	else
	{
		path = msg._sender + SEPARATOR + msg._receiver + ".txt";
	}

	// checking if there is content in the messsage
	if (msg._content.size() > 0)
	{
		file.open(path, std::ofstream::out | std::ofstream::app);
		file << MSG_OPENING << msg._sender << MSG_SEPERATOR << msg._content;
	}
	
	file.close();
	return path;
}

/*
function send a new message to the clients
input: the path to the file with the content and the message with the sender and the receive
output: none
*/
void Response::sendNewMsg(std::string filePath, const Message msg)
{
	std::string content;

	std::ifstream file(filePath);
	std::getline(file, content);
	file.close();

	this->sendUpdate(msg, content);
}

/*
function send the update message to the server
input: the message to send with the content after putting the protocol
output: none
*/
void Response::sendUpdate(const Message msg, const std::string content)
{
	std::string allUsers = this->getUsers();
	Helper::send_update_message_to_client(this->_users[msg._receiver], content, msg._sender, allUsers);	
}

/*
function disconnect the user from the users
input: the msg with the sender
output: none
*/
void Response::disconnected(const Message msg)
{
	std::cout << "REMOVED " << this->_users[msg._sender] << ", " << msg._sender << " from clients list" << std::endl;
	closesocket(this->_users[msg._sender]);
	this->_users.erase(msg._sender);

	// checking if there are users to send
	if (this->_users.size() > 0)
	{
		this->sendAllUsers();
	}
}