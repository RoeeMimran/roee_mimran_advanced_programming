#include "Clients.h"

// c'tor
Clients::Clients(std::map<std::string, SOCKET>& users, std::condition_variable& usersCV, std::mutex& usersMtx, std::queue<std::string>& newUsers)
	: _users(users), _usersCV(usersCV), _usersMtx(usersMtx), _newUsers(newUsers), _response(this->_messages, users)
{
}

/*
function manage all the clients, create new clients and start their threads
input: none
output: none
*/
void Clients::startChat()
{
	std::thread responseThread(&Response::messagesSender, &this->_response, std::ref(this->_msgMtx), std::ref(this->_usersMtx), std::ref(this->_msgCV));
	responseThread.detach();
	std::vector<ClientManager> clients;

	while (true)
	{
		std::unique_lock<std::mutex> locker(this->_usersMtx);
		
		// wait for new client
		this->_usersCV.wait(locker, [this]() {return !this->isNewUsersEmpty(); });

		while (!this->isNewUsersEmpty())
		{
			// create a new client and start their thread
			clients.push_back(ClientManager(this->_msgMtx, this->_messages, this->_users[this->_newUsers.front()], this->_newUsers.front()));
			std::thread clientThread(&ClientManager::receiveMessages, clients.back(), std::ref(this->_msgMtx), std::ref(this->_msgCV));
			clientThread.detach();

			this->_newUsers.pop();
		}
		locker.unlock();
	}
}

/*
getter for checkinf the newUsers
input: none
output: empty or not
*/
bool Clients::isNewUsersEmpty()
{
	return this->_newUsers.empty();
}
