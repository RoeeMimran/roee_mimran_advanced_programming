#pragma once
#include "Server.h"
#include "Clients.h"
#include "Configuration.h"

class MagshiChat
{
private:
	Clients _clients;
	Server _server;

	std::queue<std::string> _newUsers;
	std::map<std::string, SOCKET> _users;
	std::mutex _userMtx;
	std::condition_variable _usersCV;

public:
	MagshiChat();
	void magshiChat();
};

