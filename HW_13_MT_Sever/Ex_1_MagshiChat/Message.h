#pragma once

#include <string>

class Message
{
public:
	std::string _sender;
	std::string _receiver;
	std::string _content;

	Message(std::string _sender, std::string _receiver, std::string _content);
};

