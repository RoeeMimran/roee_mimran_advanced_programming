#include "MagshiChat.h"

// c'tor
// initiallize the serve and clients
MagshiChat::MagshiChat() : _server(this->_users, this->_userMtx, this->_usersCV, this->_newUsers),
	_clients(this->_users, this->_usersCV, this->_userMtx, this->_newUsers)
{
	
}

/*
function start the threads of the server and the client
input: none
output: none
*/
void MagshiChat::magshiChat()
{
	std::thread serverThread(&Server::serve, &this->_server, Configuration::getPort());
	std::thread clientsThread(&Clients::startChat, &this->_clients);
	
	serverThread.join();
	clientsThread.join();
}
