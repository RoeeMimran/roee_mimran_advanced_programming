#pragma once
#include "Response.h"
#include "ClientManager.h"

class Clients
{
private:
	std::queue<Message> _messages;
	std::map<std::string, SOCKET>& _users;
	std::mutex _msgMtx;
	std::condition_variable _msgCV;
	std::condition_variable& _usersCV;
	std::mutex& _usersMtx;
	std::queue<std::string>& _newUsers;
	Response _response;

public:
	Clients(std::map<std::string, SOCKET>& users, std::condition_variable& usersCV, std::mutex& usersMtx, std::queue<std::string>& newUsers);
	void startChat();

	bool isNewUsersEmpty();
};

