#pragma once
#include "WSAInitializer.h"
#include "Helper.h"
#include <map>
#include <mutex>
#include <thread>
#include <queue>

#define SIZE_OF_LEN 2
#define SEPARATOR '&'

class Server
{
public:
	Server(std::map<std::string, SOCKET>& users, std::mutex& usersMtx, std::condition_variable& usersCV, std::queue<std::string>& newUsers);
	~Server();
	void serve(int port);

private:
	void accept();
	void login(SOCKET clientSocket);
	void sendLoginMSg();

	WSAInitializer _WSAInitializer;
	SOCKET _serverSocket;
	
	std::map<std::string, SOCKET>& _users;
	std::mutex& _userMtx;
	std::condition_variable& _usersCV;
	std::queue<std::string>& _newUsers;
};

