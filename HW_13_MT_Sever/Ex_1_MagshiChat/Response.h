#pragma once

#include "Server.h"
#include "Message.h"
#include <fstream>
#include <string>
#include <iostream>

#define MSG_OPENING "&MAGSH_MESSAGE&&Author&"
#define MSG_SEPERATOR "&DATA&"
#define QUIT "q"

class Response
{
private:
	std::queue<Message>& _messages;
	std::map<std::string, SOCKET>& _users;

	std::string saveChat(const Message msg);
	void sendNewMsg(std::string filePath, const Message msg);
	void sendUpdate(const Message msg, const std::string content);
	void disconnected(const Message msg);
	void sendAllUsers();

public:
	Response(std::queue<Message>& messages, std::map<std::string, SOCKET>& users);
	void messagesSender(std::mutex& msgMtx, std::mutex& _userMtx, std::condition_variable& cv);

	std::string getUsers();

	bool isMessagesEmpty();
};

