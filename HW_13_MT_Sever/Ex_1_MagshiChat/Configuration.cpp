#include "Configuration.h"

/*
function get the port according to the file
input: none
output: the port
*/
int Configuration::getPort()
{
	std::ifstream config;
	config.open(CONFIG_FILE_NAME);

	std::string portLine;
	int port = 0;
	
	std::getline(config, portLine); // ip line
	std::getline(config, portLine); // port line
	
	// read line until the =
	int i = 0;
	for (i; i < portLine.size(); i++)
	{
		if (portLine[i] == '=')
		{
			i++;
			break;
		}
	}

	// read the the number and make him a number
	int j = 1;
	for (i; i < portLine.size(); i++, j++)
	{
		port = port * 10;
		port += portLine[i] - '0';
	}

	return port;
}
