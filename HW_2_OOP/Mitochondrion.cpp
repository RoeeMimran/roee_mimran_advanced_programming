#include "Mitochondrion.h"

/*
function initallize the mitocndrion
input: the object mitochondrion
output: none
*/
void Mitochondrion::init()
{
	_glocuse_level = 0;
	_has_glocuse_receptor = false;
}

/*
function check if the protein list is the wanted one
input: the head of the protein list
output: none
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* currNode = protein.get_first(); //  get the first node

	if (currNode->get_data() == ALANINE)
	{
		currNode = currNode->get_next();
		if (currNode->get_data() == LEUCINE)
		{
			currNode = currNode->get_next();
			if (currNode->get_data() == GLYCINE)
			{
				currNode = currNode->get_next();
				if (currNode->get_data() == HISTIDINE)
				{
					currNode = currNode->get_next();
					if (currNode->get_data() == LEUCINE)
					{
						currNode = currNode->get_next();
						if (currNode->get_data() == PHENYLALANINE)
						{
							currNode = currNode->get_next();
							if (currNode->get_data() == AMINO_CHAIN_END)
							{
								_has_glocuse_receptor = true;
							}
						}
					}
				}
			}
		}
	}
}

/*
function set the glucose fo the wanted one
input: the wanted gloucse
output: none
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

/*
function check if atp can be produced
input: none
output: true if atp can be produced and false if not
*/
bool Mitochondrion::produceATP() const
{
	return (_has_glocuse_receptor) && (_glocuse_level >= MIN_GLOCUSE_LEVEL);
}