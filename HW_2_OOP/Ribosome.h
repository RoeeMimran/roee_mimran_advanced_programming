#pragma once
#include "Protein.h"

#define CODING_LETTERS 3

class Ribosome
{
public:
	// methods
	Protein* create_protein(std::string& RNA_transcript) const;
};

