#pragma once

#include "Cell.h"

class Virus
{
private:
	//fields
	std::string _RNA_sequence;

public:
	//methods
	void init(const std::string RNA_sequence);
	void infect_cell(Cell& cell) const;
};