#include "Nucleus.h"

/*
function init the gene with the specific values
input: the gene fields and the gene object
output: none
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->set_start(start);
	this->set_end(end);
	this->set_is_on_complementary_dna_strand(on_complementary_dna_strand);
}

unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

void Gene::set_start(const unsigned int start)
{
	this->_start = start;
}

void Gene::set_end(const unsigned int end)
{
	this->_end = end;
}

void Gene::set_is_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
function initialize the nucleus object and get the compemntary dna strand
input: the dna string and the object nucleus
output: none
*/
void Nucleus::init(const std::string dna_sequence)
{
	_DNA_strand = dna_sequence;
	_complementary_DNA_strand = "";
	int i = 0;
	
	for (i = 0; dna_sequence[i]; i++) // replace the dna letters with the complemntary letters
	{
		switch (dna_sequence[i])
		{
		case 'A':
			_complementary_DNA_strand += 'T';
			break;

		case 'T':
			_complementary_DNA_strand += 'A';
			break;

		case 'G':
			_complementary_DNA_strand += 'C';
			break;

		case 'C':
			_complementary_DNA_strand += 'G';
			break;

		default:
			std::cout << "unvalid dna" << std::endl;
			system("PAUSE");
			exit(1);
			break;
		}
	}
}

/*
function cut the DNA to the RNA according to the gene
input: the dna and the gene
output: the rna string
*/
std::string cutDnaToRna(std::string DNA, const Gene& gene)
{
	return DNA.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1);
}

/*
function get the rna transcript from the gene and the the dna
input: the gene as a reference and the object nucleus
output: the rna transcript
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA_transcript = "";
	int i = 0;

	if (gene.is_on_complementary_dna_strand()) // checking where is the rna
	{
		RNA_transcript = cutDnaToRna(_complementary_DNA_strand, gene);
	}
	else
	{
		RNA_transcript = cutDnaToRna(_DNA_strand, gene);
	}

	for (i = 0; RNA_transcript[i]; i++) // checking for 'T' letters and switch them to 'U'
	{
		if (RNA_transcript[i] == 'T')
		{
			RNA_transcript[i] = 'U';
		}
	}
	return RNA_transcript;
}

/*
function revers the dna strand letters
input: the object Nucleus
output: the reversed string
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string tempString = _DNA_strand;
	std::reverse(tempString.begin(), tempString.end());
	
	return tempString;
}


/*
function count the number of times that the codon repeat in the dna
input: the string of the codon and the object nucleus
output: the number of times the codon repeat in the dna
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	std::string tempString = _DNA_strand;
	unsigned int numOfCodons = 0;
	unsigned int startingIndex = tempString.find(codon); 

	while (startingIndex <= _DNA_strand.size()) // if starting index bigger than dna size, the codon doesn't exist int the string
	{
		numOfCodons++;

		tempString.erase(startingIndex, codon.size()); // remove the codon from the string for the next check

		startingIndex = tempString.find(codon); // checking if there is still a codon in the dna
	}

	return numOfCodons;
}

//dna setter
void Nucleus::set_dna(const std::string DNA_strand)
{
	this->_DNA_strand = DNA_strand;
}

//dna getter
std::string Nucleus::get_DNA()
{
	return this->_DNA_strand;
}
