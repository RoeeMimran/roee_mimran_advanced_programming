#pragma once
#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"

#define MAX_ATP 100

class Cell
{
private:
	//fields
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;

public:
	//methods
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();

	//setters
	void set_dna(const std::string DNA_strand);

	//getter
	std::string get_DNA();
};
