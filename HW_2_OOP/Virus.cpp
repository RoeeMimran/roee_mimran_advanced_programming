#include "Virus.h"

/*
function initallize the virus
input: the new rna string
output: none
*/
void Virus::init(const std::string RNA_sequence)
{
	this->_RNA_sequence = RNA_sequence;
}

/*
function infect the cell and insert the virus to the dna string
input: the cell object with the dna
output: none
*/
void Virus::infect_cell(Cell& cell) const
{
	std::string original_DNA = cell.get_DNA(); // get the dna
	int virusStartIndex = original_DNA.size() / 2; // get the size of the dna and divide by 2 for the virus place

	original_DNA.insert(virusStartIndex, this->_RNA_sequence);
	cell.set_dna(original_DNA);
}
