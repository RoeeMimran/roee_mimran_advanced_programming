#include "Ribosome.h"

/*
function create the linked list, the protein
input:: the rna transcript and the object ribosome
output: the head of the list of the protein
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	std::string codingStr = "";
	AminoAcid aminoToAdd;
	int i = 0;

	Protein* protein = new Protein; // create the linked list
	protein->init();

	while (((RNA_transcript.size() - i) / CODING_LETTERS >= 1) && protein) // untill there are less than 3 letters or thr amino acid is unknown
	{
		codingStr = RNA_transcript.substr(i, CODING_LETTERS); // get the 3 letters of the coding
		aminoToAdd = get_amino_acid(codingStr);

		if (aminoToAdd == UNKNOWN)
		{
			protein->clear();
			protein = nullptr;
		}
		else
		{
			protein->add(aminoToAdd);
			i += CODING_LETTERS;
		}
	}
	return protein;
}
