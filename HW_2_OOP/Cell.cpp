#include "Cell.h"

/*
function initialllize the object that inside the cell
input: the dna and the gene
output: none
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_glocus_receptor_gene.init(glucose_receptor_gene.get_start(), glucose_receptor_gene.get_end(), glucose_receptor_gene.is_on_complementary_dna_strand());
	_nucleus.init(dna_sequence);
	_mitochondrion.init();
}

/*
function check if the cell can proudce atp
input: none
output: true if atp can be prudced, false if not
*/
bool Cell::get_ATP()
{
	bool isAbleToPrudceAtp = false;
	Protein* firstHead = 0;
	std::string RNA = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);// get the rna

	firstHead = this->_ribosome.create_protein(RNA); // create the linked list

	if (!firstHead) // checking if there was a problem
	{
		std::cout << "the protein couldn't be created" << std::endl;
		system("PAUSE");
		_exit(1);
	}
	
	this->_mitochondrion.insert_glucose_receptor(*firstHead); // checking if the protein is the wanted protein
	this->_mitochondrion.set_glucose(MIN_GLOCUSE_LEVEL);

	isAbleToPrudceAtp = this->_mitochondrion.produceATP(); // checking if atp can be prudced

	if (isAbleToPrudceAtp)
	{
		this->_atp_units = MAX_ATP;
	}

	firstHead->clear(); // clear the memory
	return isAbleToPrudceAtp;
}

// dna setter
void Cell::set_dna(const std::string DNA_strand)
{
	this->_nucleus.set_dna(DNA_strand);
}

// dna getter
std::string Cell::get_DNA()
{
	return this->_nucleus.get_DNA();
}
