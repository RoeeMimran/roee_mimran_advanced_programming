#pragma once
#include "Ribosome.h"

#define MIN_GLOCUSE_LEVEL 50

class Mitochondrion
{
public:
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;

private:
	//fields
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};