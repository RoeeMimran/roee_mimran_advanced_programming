#include "String.h"

String::String(std::string string)
{
	this->_string = string;
}

String::~String()
{
}

bool String::isPrintable() const
{
	return true;
}

std::string String::toString() const
{
	return this->_string;
}
