#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"

class Boolean : public Type
{
private:
	bool _boolean;

public:
	Boolean(bool boolean);
	~Boolean();

	virtual bool isPrintable() const;
	virtual std::string toString() const;
};

#endif // BOOLEAN_H