#include "Integer.h"

Integer::Integer(int num)
{
	this->_num = num;
}

Integer::~Integer()
{
}

bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	std::string numberInStr;
	int temp = this->_num;
	char digit = 0;

	while(temp > 0)
	{
		digit = temp % 10 + '0';
		numberInStr = digit + numberInStr;
		temp = temp / 10;
	}

	Helper::removeLeadingZeros(numberInStr);
	return numberInStr;
}
