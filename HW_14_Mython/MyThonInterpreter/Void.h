#ifndef VOID_H
#define VOID_H

#include "type.h"

class Void : public Type
{
public:
	Void();
	~Void();

	virtual bool isPrintable() const;
	virtual std::string toString() const;
};

#endif // VOID_H