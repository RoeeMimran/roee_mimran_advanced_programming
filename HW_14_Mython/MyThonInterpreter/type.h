#ifndef TYPE_H
#define TYPE_H

#include <string>
#include "Helper.h"

class Type
{
private:
	bool _isTemp;

public:
	Type();
	~Type();

	bool getIsTemp() const;
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
};





#endif //TYPE_H
