#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"

class Integer : public Type
{
private:
	int _num;

public:
	Integer(int num);
	~Integer();

	virtual bool isPrintable() const;
	virtual std::string toString() const;
};

#endif // INTEGER_H