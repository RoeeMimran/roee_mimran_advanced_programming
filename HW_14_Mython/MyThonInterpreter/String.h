#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence
{
private:
	std::string _string;

public:
	String(std::string);
	~String();

	virtual bool isPrintable() const;
	virtual std::string toString() const;
};

#endif // STRING_H