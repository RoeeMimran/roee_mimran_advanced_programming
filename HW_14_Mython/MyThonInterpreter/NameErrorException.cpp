#define _CRT_SECURE_NO_WARNINGS
#include "NameErrorException.h"

NameErrorException::NameErrorException(std::string name)
{
	this->_name = name;
}

const char* NameErrorException::what() const throw()
{
	std::string str = "NameError: name '["  + this->_name + "]' is not defined";
	char* msg = new char[str.size()];

	strcpy(msg, str.c_str());
	return msg;
}
