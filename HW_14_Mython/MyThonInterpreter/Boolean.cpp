#include "Boolean.h"

Boolean::Boolean(bool boolean)
{
	this->_boolean = boolean;
}

Boolean::~Boolean()
{
}

bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	if (this->_boolean)
	{
		return "True";
	}
	else
	{
		return "False";
	}
}
