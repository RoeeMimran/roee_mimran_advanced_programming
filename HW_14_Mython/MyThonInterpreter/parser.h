#ifndef PARSER_H
#define PARSER_H

#include <unordered_map>
#include "InterperterException.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include "SyntaxException.h"
#include "IndentationException.h"
#include "NameErrorException.h"


class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string& str);
	static void deleteVars();

private:
	static bool isLegalVarName(const std::string& str);
	static bool makeAssignment(const std::string& str);
	static Type* getVariableValue(const std::string& str);
	static bool copy(const std::string& str);

	static std::unordered_map<std::string, Type*> _variables;
};

#endif //PARSER_H
