#include "parser.h"
#include <iostream>

std::unordered_map<std::string, Type*> Parser::_variables;

Type* Parser::parseString(std::string str) throw()
{
	Type* newType = nullptr;

	// check for existing value
	Type* existing = Parser::getVariableValue(str);
	if (existing != 0)
	{
		std::cout << existing->toString() << std::endl;
	}
	else
	{
		if (str.length() > 0)
		{
			if (str[0] == ' ' || str[0] == '\t')
			{
				throw IndentationException();
			}

			Helper::rtrim(str); // remove last sapces
			newType = getType(str);

			// checking if the type is exist
			if (!newType)
			{
				if (!copy(str))
				{
					if (!makeAssignment(str))
					{
						throw NameErrorException(str);
					}
				}
			}
			else
			{
				if (newType->isPrintable())
				{
					std::cout << newType->toString() << std::endl;
				}

				if (newType->getIsTemp())
				{
					delete newType;
					newType = nullptr;
				}
			}
		}
	}
	return newType;
}

Type* Parser::getType(std::string& str)
{
	Type* newType = 0;

	if (Helper::isInteger(str))
	{
		int num = 0;
		for (int i = 0; i < str.size(); i++)
		{
			num = num * 10 + str[i] - '0';
		}
		newType = new Integer(num);
	}
	else if (Helper::isBoolean(str))
	{
		newType = new Boolean(str == "True" ? true : false);
	}
	else if (Helper::isString(str))
	{
		newType = new String(str);
	}
	else
	{
		newType = nullptr;
	}

	return newType;
}

bool Parser::isLegalVarName(const std::string& str)
{
	bool legal = true;

	// check for first char
	if (!(Helper::isLetter(str[0]) || Helper::isUnderscore(str[0])))
	{
		legal = false;
	}

	// check for all the others
	for (int i = 1; i < str.size() && legal; i++)
	{
		if (!(Helper::isLetter(str[i]) || Helper::isUnderscore(str[i]) || Helper::isDigit(str[i])))
		{
			legal = false;
		}
	}
	return legal;
}

bool Parser::makeAssignment(const std::string& str)
{
	std::string name = str.substr(0, str.find("="));
	std::string variable = str.substr(str.find("=") + 1, str.size());

	if (name == str)
	{
		if (!isLegalVarName(name))
		{
			throw SyntaxException();
		}
		else
		{
			throw NameErrorException(name);
		}
	}

	Helper::trim(name);
	Helper::trim(variable);

	if (!isLegalVarName(name))
	{
		throw SyntaxException();
	}

	// looking for the type of the variable
	Type* newType = getType(variable);
	if (newType == 0)
	{
		throw SyntaxException();
	}

	// checking if the variable exist
	if (Parser::_variables.find(name) != _variables.end())
	{
		Parser::_variables.erase(name);
	}

	std::pair<std::string, Type*> newVar(name, newType);
	Parser::_variables.insert(newVar);

	return true;	
}

Type* Parser::getVariableValue(const std::string& str)
{
	auto it = Parser::_variables.find(str);
	if (it == Parser::_variables.end())
	{
		return 0;
	}
	else
	{
		return it->second;
	}
}

bool Parser::copy(const std::string& str)
{
	std::string dst = str.substr(0, str.find("="));
	std::string src = str.substr(str.find("=") + 1, str.size());
	Helper::trim(src);
	Helper::trim(dst);
	if (dst == str)
	{
		return false;
	}

	if (Helper::isBoolean(dst) || Helper::isString(dst) || Helper::isInteger(dst))
	{
		return false;
	}

	// check for existens
	if (_variables.find(dst) != _variables.end() && _variables.find(dst) != _variables.end())
	{
		_variables.erase(dst);

		auto it = _variables.find(src);
		std::string newTypeStr = it->second->toString();
		
		Type* newType = getType(newTypeStr);
		std::pair<std::string, Type*> newVar(dst, newType);
		Parser::_variables.insert(newVar);
		return true;
	}
	else
	{
		return false;
	}
}

void Parser::deleteVars()
{
	// delete the located memory and erase him from the map
	for (auto it = _variables.begin(); it != _variables.end(); ++it)
	{
		delete it->second;
	}

}
