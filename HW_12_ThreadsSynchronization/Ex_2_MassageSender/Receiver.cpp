#include "Receiver.h"

/*
c'tor
function initialized the the mutex and the condition variable and create the file
input: reference of the mutex and the condition variable to copy
output: none
*/
Receiver::Receiver(std::mutex& mtx, std::condition_variable& cv) : _mtx(mtx), _cv(cv)
{
	// checking if a file exist
	this->_data.open(DATA_NAME, std::fstream::in | std::fstream::out | std::fstream::app);
	if (!this->_data)
	{
		// create the file
		this->_data.open(DATA_NAME);
	}
	this->_data.close();
}

/*
function read the file and erase his content
input: the queue of messgaes and the choice of the user to know when to exit
output: none
*/
void Receiver::readFromFile(std::queue<std::string>& messages, int* choice)
{
	while (*choice != EXIT)
	{
		std::unique_lock<std::mutex> locker(this->_mtx);

		// read the file and clean it
		this->read(messages);
		this->clean();

		// wake up the sender thread
		locker.unlock();
		this->_cv.notify_all();

		std::this_thread::sleep_for(std::chrono::seconds(TIME_TO_WAIT));
	}
}

/*
function read the data from the file and add every line to the queue
input: the queue to add 
output: none
*/
void Receiver::read(std::queue<std::string>& messages)
{
	std::string line;
	this->_data.open(DATA_NAME, std::fstream::in | std::fstream::app);
	
	// read each line and push it to the queue
	while (std::getline(this->_data, line))
	{
		messages.push(line);
	}
}

/*
function erase the content of the file
input: none
output: none
*/
void Receiver::clean()
{
	// close the current file
	this->_data.close();
	this->_data.open(DATA_NAME, std::ios::out | std::ios::trunc); // open it with trunc so it will erase it
	this->_data.close();
}
