#include "MessageSender.h"

void MessageSender::massageSender()
{
	int choice = 0;
	std::queue<std::string> messages;

	FileHandler fileHandler;
	fileHandler.manager(this->_users, &choice);

	while (choice != EXIT)
	{
		choice = mainMenu();

		switch (choice)
		{
		case SIGN_IN:
			this->signIn();
			break;

		case SIGN_OUT:
			this->signOut(fileHandler.getMtx());
			break;

		case CONNECTED_USERS:
			this->printAllUsers();
			break;

		case EXIT:
			std::cout << GOODBYE_MSG << std::endl;
			break;

		default:
			std::cout << CHOICE_ERROR << std::endl;
			break;
		}

		system("PAUSE");
		system("CLS");
	}
}

/*
function print the main menu of the program and ask for his choice
input: none
oupute the user choice
*/
int MessageSender::mainMenu()
{
	int choice = 0;

	// print the choices
	std::cout << "1. sign in" << std::endl;
	std::cout << "2. sign out" << std::endl;
	std::cout << "3. Connected Users" << std::endl;
	std::cout << "4. exit" << std::endl;

	// ask for his choice
	std::cin >> choice;
	return choice;
}

/*
function sign in a new user and add him to the set of users
input: none
output: none
*/
void MessageSender::signIn()
{
	std::string newUser;

	std::cout << "your name: " << std::endl;
	std::cin >> newUser;

	// checking if the user is already exist
	if (this->_users.find(newUser) == this->_users.end())
	{
		this->_users.insert(newUser);
		std::cout << NEW_USER << std::endl;
	}
	else
	{
		std::cout << USER_EXIST << std::endl;
	}
}

/*
function print all the user in the set
input: none
output: none
*/
void MessageSender::printAllUsers()
{
	std::set<std::string>::iterator it;
	std::cout << "user:" << std::endl;

	// pass on everyone in the set and print his name
	for (it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		std::cout << *it << std::endl;
	}
	std::cout << std::endl;
}

/*
function sign out the user and remove him from the set of users
input: the mutex of sender
output: none
*/
void MessageSender::signOut(std::mutex& mtx)
{
	std::string name;

	// ask for his name
	std::cout << "your name: " << std::endl;
	std::cin >> name;

	std::set<std::string>::iterator it = this->_users.find(name);

	// lock the users
	std::unique_lock<std::mutex> locker(mtx);

	// checking if ther user doesn;t exist
	if (it == this->_users.end())
	{
		std::cout << USER_NOT_EXIST_ERROR << std::endl;
	}
	else
	{
		this->_users.erase(it);
	}

	locker.unlock();
}
