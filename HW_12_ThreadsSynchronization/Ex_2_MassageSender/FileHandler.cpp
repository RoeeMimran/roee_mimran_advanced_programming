#include "FileHandler.h"

FileHandler::FileHandler() : _receiver(this->_mtx, this->_cv), _sender(this->_mtx, this->_cv)
{
}

/*
function start the 2 threads
input: thhe users and the choice of the user
output: none
*/
void FileHandler::manager(std::set<std::string>& users, int* choice)
{
	std::thread receiverThread(&Receiver::readFromFile, &this->_receiver, std::ref(this->_messages), choice);
	std::thread senderThread(&Sender::writeMessages, &this->_sender, std::ref(this->_messages), std::ref(users), choice);

	receiverThread.detach();
	senderThread.detach();
}

std::mutex& FileHandler::getMtx()
{
	return this->_mtx;
}
