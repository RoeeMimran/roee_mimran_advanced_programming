#pragma once
#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include "Sender.h"

#define DATA_NAME "data.txt"
#define TIME_TO_WAIT 60

// class responsible about reading the messages and add it to the queue
class Receiver
{
public:
	Receiver(std::mutex& mtx, std::condition_variable& cv);
	void readFromFile(std::queue<std::string>& messages, int* choice);

private:
	void read(std::queue<std::string>& messages);
	void clean();

	std::fstream _data;

	std::mutex& _mtx;
	std::condition_variable& _cv;
};
