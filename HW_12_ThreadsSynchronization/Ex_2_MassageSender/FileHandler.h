#pragma once

#include "Receiver.h"
#include "Sender.h"

// class create 2 threads of receiver and sender 
class FileHandler
{
public:
	FileHandler();
	void manager(std::set<std::string>& users, int* choice);

	std::mutex& getMtx();

private:
	Receiver _receiver;
	Sender _sender;

	std::queue<std::string> _messages;

	// locks:
	std::mutex _mtx;
	std::condition_variable _cv;
};