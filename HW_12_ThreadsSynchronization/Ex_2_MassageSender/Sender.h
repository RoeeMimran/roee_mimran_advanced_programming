#pragma once
#include <set>
#include <iostream>
#include <queue>
#include <fstream>
#include <mutex>
#include <condition_variable>

#define OUTPUT_NAME "output.txt"
#define EXIT 4
#define SENDING_MSG "wrote messages to output file"

// class responsible about sending the messages to the file
class Sender
{
public:
	Sender(std::mutex& mtx, std::condition_variable& cv);
	void writeMessages(std::queue<std::string>& messages, std::set<std::string>& users, int* choice);

private:
	std::ofstream _output;

	std::mutex& _mtx;
	std::condition_variable& _cv;
};