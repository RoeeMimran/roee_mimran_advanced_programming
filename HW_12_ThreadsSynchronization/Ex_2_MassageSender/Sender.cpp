#include "Sender.h"

Sender::Sender(std::mutex& mtx, std::condition_variable& cv) : _mtx(mtx), _cv(cv)
{
	// create a file with his name
	this->_output.open(OUTPUT_NAME);
}

/*
function write the messages to the file
input: the messages and the users, the choice to know when to exit from the thread
output: none
*/
void Sender::writeMessages(std::queue<std::string>& messages, std::set<std::string>& users, int* choice)
{
	std::set<std::string>::iterator it;

	while (*choice != EXIT)
	{
		std::unique_lock<std::mutex> locker(this->_mtx);

		// wait for receiver thread to finish read
		this->_cv.wait(locker, [&messages]() { return !messages.empty(); });

		// write every message to the file
		while (!messages.empty())
		{
			// pass on every user and write the message from his name
			for (it = users.begin(); it != users.end(); ++it)
			{
				this->_output << *it << ": " << messages.front() << std::endl;
			}
			// remove the message from the queue and go to the nex message
			messages.pop();
		}
		locker.unlock();
		std::cout << SENDING_MSG << std::endl;
	}
}
