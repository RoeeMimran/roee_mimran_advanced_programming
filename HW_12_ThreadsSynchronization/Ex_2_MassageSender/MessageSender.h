#pragma once
#include <set>
#include <iostream>
#include "FileHandler.h"
#include <queue>

#define GOODBYE_MSG "bye bye"
#define CHOICE_ERROR "choose one of the options above"
#define USER_EXIST "the user is already exisy"
#define USER_NOT_EXIST_ERROR "the user doesn't exist"
#define NEW_USER "welocme to message sender"

enum choices
{
	SIGN_IN = 1,
	SIGN_OUT,
	CONNECTED_USERS,
};

class MessageSender
{
public:
	void massageSender();

private:
	int mainMenu();
	void signIn();
	void printAllUsers();
	void signOut(std::mutex& mtx);

	std::set<std::string> _users;
};