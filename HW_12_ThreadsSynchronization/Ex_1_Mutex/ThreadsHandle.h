#pragma once
#include <vector>
#include "threads.h"

class ThreadsHandle
{
public:
	void addThread(int begin, int end, std::ofstream& file);
	void start();

private:
	std::vector<std::thread> _threads;
};