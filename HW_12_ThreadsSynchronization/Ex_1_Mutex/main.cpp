#include "threads.h"

int main()
{
	call_I_Love_Threads();
	
	std::vector<int> primes1;
	getPrimes(58, 100, primes1);

	std::vector<int> primes3 = callGetPrimes(1, 100);
	primes3 = callGetPrimes(0, 1000);
	primes3 = callGetPrimes(0, 100000);
	primes3 = callGetPrimes(0, 1000000);
	
	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 100);

	callWritePrimesMultipleThreads(1, 1000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 4);
	callWritePrimesMultipleThreads(1, 1000000, "primes2.txt", 15);
	
	system("pause");
	return 0;
}