#pragma once
#include "ThreadsHandle.h"

/*
function add the thread to the vector with the params
input: the params for the writePrimeToFile function
output: noen
*/
void ThreadsHandle::addThread(int begin, int end, std::ofstream& file)
{
	this->_threads.push_back(std::thread(writePrimesToFile, begin, end, std::ref(file)));
}

/*
function join all the threads and start them
input: none
output: none
*/
void ThreadsHandle::start()
{
	for (unsigned int i = 0; i < this->_threads.size(); ++i)
	{
		this->_threads[i].join();
	}
}
