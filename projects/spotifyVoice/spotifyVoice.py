import speech_recognition as sr
import spotipy
import spotipy.util as util
import sys

CLIENT_ID = '0dddbf83b3fc40e184471d94739ec33b'
CLIENT_SECRET = '5f8f2f3d4f6d4547ba9ff350b613feef'
REDIRECT_URI = 'http://voice.com'
USERNAME = 'imee30tnivo9qt2pxnhglwa8q'
SCOPE = 'user-modify-playback-state'

pause = ['pause', 'pose', 'stop', 'stock', 'stuff', 'buzz', 'doves']
back = ['back', 'beck', 'baked', 'bass', 'bic', 'lake', 'make', 'bec']
next = ['next', 'text']

def hanle_request(sp, text):
    if text == 'spotify':
        sp.start_playback()
    elif text in pause:
        sp.pause_playback()
    elif text in next:
        sp.next_track()
    elif text in back:
        sp.previous_track()
    elif text == 'shuffle':
        sp.shuffle(True)
    elif text in 'not shuffle':
        sp.shuffle(False)
    else:
        print('not spotify')

def from_text_to_requests(text):
    requests = text.split(' ')
    print(requests)
    return requests


def spotify_connect():
    token = util.prompt_for_user_token(USERNAME,
                                SCOPE,
                                client_id=CLIENT_ID,
                                client_secret=CLIENT_SECRET,
                                redirect_uri=REDIRECT_URI)
    if token:
        print('connected')
        sp = spotipy.Spotify(auth=token)
    else:
        print("Can't get token for", USERNAME) 
        sp = 0

    return sp


def main():
    sp = spotify_connect()
    if not sp == 0:
        r = sr.Recognizer()
        r.pause_threshold = 0.1  # seconds of non-speaking audio before a phrase is considered complete
        r.phrase_threshold = 0.3  # minimum seconds of speaking audio before we consider the speaking audio a phrase - values below this are ignored (for filtering out clicks and pops)
        r.non_speaking_duration = 0.1  # seconds of non-speaking audio to keep on both sides of the recording

        with sr.Microphone(device_index=1) as source: 
#            r.adjust_for_ambient_noise(source)

            while True:
                try:
                    print('speak:')
                    audio = r.listen(source, timeout=1)

                    text = r.recognize_google(audio).lower()

                    requests = from_text_to_requests(text)

                    if text == 'repeat':
                        requests.append('beck')
                        requests.append('next')
                    elif requests[0] == 'volume':
                        sp.volume(int(requests[1]))
                        requests.remove(requests[0])
                        requests.remove(requests[0])

                    for request in requests: 
                        hanle_request(sp, request)

                except spotipy.client.SpotifyException:
                    print('trying to connect again')
                    sp = spotify_connect()
                except Exception:
                    print('didnt hear u', sys.exc_info()[0]) 


if __name__ == "__main__":
    main()