import mosaic


def compare_pixel(pixel1, pixel2):
    return abs(pixel1[0] - pixel2[0]) + abs(pixel1[1] - pixel2[1]) + abs(pixel1[2] - pixel2[2])


def compare(image1, image2):
    sum = 0
    for i in range(len(image1)):
        for j in range(len(image1[i])):
            if i < len(image2) and j < len(image2[i]):
                sum += compare_pixel(image1[i][j], image2[i][j])
            else:
                break
    return sum


def get_piece(image, upper_left, size):
    WIDTH = 1
    HEIGHT = 0
    new_image = []
    for curr_height in range(size[HEIGHT]):
        new_image.append([])
        if curr_height + upper_left[HEIGHT] < len(image):  # checking for height borders
            for curr_width in range(size[WIDTH]):
                if curr_width + upper_left[WIDTH] < len(image[curr_height]):  # checking for width borders
                    new_image[curr_height].append(
                        image[upper_left[HEIGHT] + curr_height][upper_left[WIDTH] + curr_width])
                else:
                    # width out of bounds
                    break
        else:
            # height out of bounds
            break
    return new_image


def set_piece(image, upper_left, piece):
    WIDTH = 1
    HEIGHT = 0
    for curr_height in range(len(piece)):
        if curr_height + upper_left[HEIGHT] < len(image):  # checking for height borders
            for curr_width in range(len(piece[curr_height])):
                if curr_width + upper_left[WIDTH] < len(image[curr_height]):  # checking for width borders
                    image[upper_left[HEIGHT] + curr_height][upper_left[WIDTH] + curr_width] = piece[curr_height][
                        curr_width]
                else:
                    # width out of bounds
                    break
        else:
            # height out of bounds
            break


def average(image):
    red_sum = 0
    green_sum = 0
    blue_sum = 0
    num_of_pixels = len(image) * len(image[0])

    # sum all the colors
    for curr_row in image:
        for curr_pixel in curr_row:
            red_sum += curr_pixel[0]
            green_sum += curr_pixel[1]
            blue_sum += curr_pixel[2]

    # divide it so we will get the avg
    return int(red_sum / num_of_pixels), int(green_sum / num_of_pixels), int(blue_sum / num_of_pixels)


def preprocess_tiles(tiles):
    averages = []
    for tile in tiles:
        averages.append(average(tile))

    # convert it tuple from list
    return tuple(averages)


def subtruct_colors(wanted_avg, other):
    return compare_pixel(wanted_avg, other)


def get_best_tiles(objective, tiles, averages, num_candidates):
    best_tiles_temp = []
    best_tiles = []
    wanted_avg = average(objective)

    # put all the tiles in a list with their avg
    for i in range(len(tiles)):
        curr_avg = compare_pixel(wanted_avg, averages[i])
        best_tiles_temp.append((tiles[i], curr_avg))

    # sort the list so we will get the best
    best_tiles_temp.sort(key=lambda tup: tup[1])
    best_tiles_temp = best_tiles_temp[:num_candidates]

    # return only the list of tiles without
    for tile in best_tiles_temp:
        best_tiles.append(tile[0])

    # convert it to tuple from list
    return tuple(best_tiles)


def choose_tile(piece, tiles):
    best_tile = tiles[0]
    smallest_dist = compare(piece, best_tile)
    for tile in tiles:
        curr_dist = compare(tile, piece)
        if curr_dist < smallest_dist:
            smallest_dist = curr_dist
            best_tile = tile
    return best_tile


def make_mosaic(image, tiles, num_candidates):
    best_tiles = get_best_tiles(image, tiles, preprocess_tiles(tiles), num_candidates)
    best_tile = choose_tile(image, best_tiles)
    return best_tile


def load_files(image_source, images_dir, tile_height):
    """
    function load the tiles folder and the big image
    :param image_source: the name of the image
    :param images_dir: the name of the tiles folder
    :param tile_height: the height of the tile
    :return: the image nad the tiles
    """
    image = mosaic.load_image(image_source)
    tiles = mosaic.build_tile_base(images_dir, tile_height)
    return image, tiles


def save_image(image, output_name):
    """
    function save the image and show it
    :param image: the image after processing
    :param output_name:  the name of the file
    """
    mosaic.show(image)
    mosaic.save(image, output_name)


def main():
    image_source = 'im1.JPG'
    images_dir = 'images'
    output_name = 'newImage.JPG'
    tile_height = 40
    num_cadidates = 10

    image, tiles = load_files(image_source, images_dir, tile_height)
    finished = len(image) * len(image[0]) / tile_height  # get the amount of pixels when finish

    curr_width = 0
    curr_height = 0
    while curr_height < len(image):
        while curr_width < len(image[curr_height]):
            current = (curr_height / tile_height * len(image[0]) + curr_width)  # get the current pixels that done
            current = 100 / finished * current  # convert it to precent
            print('%.3f' % current, '%')

            # get the current piece
            curr_piece = get_piece(image, (curr_height, curr_width), (tile_height, tile_height))
            new_piece = make_mosaic(curr_piece, tiles, num_cadidates)  # replace it with the new image
            set_piece(image, (curr_height, curr_width), new_piece)

            curr_width += tile_height
        curr_height += tile_height
        curr_width = 0

    # done
    print('100 %')
    save_image(image, output_name)


if __name__ == '__main__':
    main()
