#include "BetterCanvas.h"

BetterCanvas::BetterCanvas()
{
}

BetterCanvas::~BetterCanvas()
{
}

/*
function draw shape according to the type of the shape and his color
input: the shape and his color
output: none
*/
void BetterCanvas::draw_shape(Shape* shape, Color color)
{
	std::string types[NUM_OF_TYPES] = { "circle", "arrow", "trignale", "rectangle" };
	int i = 0;
	bool found = false;

	// find the index of the shape in the types array
	for (i = 0; i < NUM_OF_TYPES && !found; i++)
	{
		if (shape->getType() == types[i])
		{
			found = true;
		}
	}
	// the for loop add 1 to i at the end of the loop so we need to decres it
	i--;

	// casting the shape to the wanted shape
	switch (i)
	{
	case CIRCLE:
		shape = (myShapes::Circle*) shape;
		break;
	case ARROW:
		shape = (myShapes::Arrow*) shape;
		break;
	case TRIANGLE:
		shape = (myShapes::Triangle*) shape;
		break;
	case RECTANGLE:
		shape = (myShapes::Rectangle*) shape;
		break;
	}

	// draw it
	shape->draw(this->_canvas);
}