#include "Arrow.h"
#include "Polygon.h"

//c'tor
myShapes::Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(name, type)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
}

//d'tor
myShapes::Arrow::~Arrow()
{
	this->_points.pop_back();
	this->_points.pop_back();
}

double myShapes::Arrow::getArea() const
{
	return 0;
}

double myShapes::Arrow::getPerimeter() const
{
	// distance of the arrow
	return this->_points[0].distance(this->_points[1]);
}

void myShapes::Arrow::move(const Point& other)
{
	addingPoints(this->_points, other);
}

void myShapes::Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1], this->getColor());
}
void myShapes::Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

/*
function initialize arrow with his fields
input: none
output: pointer of dynamic arrow
*/
myShapes::Arrow* myShapes::Arrow::initializeArrow()
{
	int i = 0;
	double x[NUM_OF_POINTS] = { 0 };
	double y[NUM_OF_POINTS] = { 0 };
	std::string name;
	
	for (i = 0; i < NUM_OF_POINTS; i++)
	{
		std::cout << "Enter the X of point numebr: " << i + 1 << std::endl;
		std::cin >> x[i];
		std::cout << "Enter the Y of point numebr: " << i + 1 << std::endl;
		std::cin >> y[i];
	}
	
	std::cout << "Enter the name of the shape:" << std::endl;
	std::cin >> name;

	return new myShapes::Arrow(Point(x[0], y[0]), Point(x[1], y[1]), "arrow", name);
}

