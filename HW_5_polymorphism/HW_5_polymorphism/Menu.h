#pragma once

#include "Triangle.h"
#include "Rectangle.h"
#include "Arrow.h"
#include "Circle.h"
#include "Canvas.h"
#include <vector>
#include <iostream>

enum choices
{
	ADD_SHAPE = 0,
	MODIFY,
	DELETE_ALL_SHAPES,
	EXIT
};

enum shapesNumber
{
	CIRCLE = 0,
	ARROW,
	TRIANGLE,
	RECTANGLE
};

enum informationAndModification
{
	MOVE_SHAPE = 0,
	INFORMATION,
	REMOVE,
	CHANGE_COLOR
};

#define LAST_MODIFICATION_OPTION 3
#define FIRST_MODIFICATION_OPTION 0

class Menu
{
public:
	Menu();
	~Menu();

	void mainMenu();

private:
	// methods
	void createNewShape(const int choice);
	void deleteAllShapes();
	void afterChangeShape();
	void moveTheShape(const int chosenShape);
	void printAllShapes();

	// menus
	Color colorMenu() const;
	void modifyMenu();
	int printMainMenu() const;
	int addShapeMenu() const;

	// fields
	Canvas _canvas;
	std::vector<Shape*> _shapes;
};