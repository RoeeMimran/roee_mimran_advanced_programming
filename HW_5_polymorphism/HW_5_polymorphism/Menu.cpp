#include "Menu.h"

Menu::Menu()
{
}

Menu::~Menu()
{
}

/*
function print the main menu and get the user input, his choice
input: none
output: his choice
*/
int Menu::printMainMenu() const
{
	int choice = 0;

	system("CLS"); // clear the screen
	std::cout << "Enter 0 to add a new shape." << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all of the shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;

	std::cin >> choice;
	return choice;
}

/*
funciton is the main loop of the program and choose the funciton to call
input: none
output: none
*/
void Menu::mainMenu()
{
	int choice = choice = this->printMainMenu();;

	while (choice != EXIT)
	{
		switch (choice)
		{
		case ADD_SHAPE:
			choice = this->addShapeMenu();
			this->createNewShape(choice);
			break;

		case MODIFY:
			if (this->_shapes.size() > 0)
			{
				// cant modify for 0 shapes
				this->modifyMenu();
			}
			break;

		case DELETE_ALL_SHAPES:
			this->deleteAllShapes();
			break;

		default:
			break;
		}
		
		choice = this->printMainMenu();
	}

	// free all the memory that we located
	this->deleteAllShapes();
}

/*
choice 0:
function print the kind of shapes that the user can paint
input: none
output: the user choice
*/
int Menu::addShapeMenu() const
{
	int choice = 0;

	system("CLS"); // clear the screen
	std::cout << "Enter 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle." << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;

	std::cin >> choice;
	return choice;
}

/*
choice 0 - create new shape
function create a new shape by the choice of the user
input: the choice of the user
output: none
*/
void Menu::createNewShape(const int choice)
{
	Shape* newShape = 0;
	bool succeded = true;

	switch (choice)
	{
	case CIRCLE:
		newShape = (Shape*)myShapes::Circle::initializeCircle();
		break;

	case ARROW:
		newShape = (Shape*)myShapes::Arrow::initializeArrow();
		break;

	case TRIANGLE:
		newShape = (Shape*)myShapes::Triangle::initializeTriangle();
		break;

	case RECTANGLE:
		newShape = (Shape*)myShapes::Rectangle::initializeRectangle();
		break;

	default:
		break;
	}
	
	// checking if the user put a valid inputs
	if (newShape)
	{
		newShape->setColor(this->colorMenu());
		
		// add the new shape to the vecotr and show it in screen
		this->_shapes.push_back(newShape);
		newShape->draw(this->_canvas);
	}
}

/*
function print the shapes for the modification and the information
input: none
output: noen
*/
void Menu::printAllShapes()
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		std::cout << "Enter " << i << " for " << this->_shapes[i]->getName() << "(" << this->_shapes[i]->getType() << ")" << std::endl;
	}
}

/*
choice 2 - modify and information
function print the menu of information and modify
input: none
output: the user choice
*/
void Menu::modifyMenu()
{
	int choice = 0;
	int chosenShape = 0;

	do {
		// checking valid shape
		system("CLS"); // clear the screen

		this->printAllShapes();
		std::cin >> chosenShape;
	} while (0 > chosenShape || chosenShape >= this->_shapes.size());

	do {
		std::cout << "Enter 0 to move the shape" << std::endl;
		std::cout << "Enter 1 to get its details." << std::endl;
		std::cout << "Enter 2 to remove the shape." << std::endl;
		std::cout << "Enter 3 to change the color of the shape" << std::endl;
		std::cin >> choice;
		// checking valid
	} while (choice < FIRST_MODIFICATION_OPTION || choice > LAST_MODIFICATION_OPTION);
	
	// check his option
	switch (choice)
	{
	case MOVE_SHAPE:
		this->moveTheShape(chosenShape);
		this->afterChangeShape();
		break;

	case INFORMATION:
		this->_shapes[chosenShape]->printDetails();
		system("PAUSE");
		break;

	case REMOVE:
		// remove it from screen
		this->_shapes[chosenShape]->clearDraw(this->_canvas);

		// delete it from the vecotr
		delete this->_shapes[chosenShape];
		this->_shapes.erase(this->_shapes.begin() + chosenShape);

		// refresh the screen
		this->afterChangeShape();
		break;

	case CHANGE_COLOR:
		// change the color and refresh the screen
		this->_shapes[chosenShape]->setColor(this->colorMenu());
		this->afterChangeShape();
	}
}

/*
function get the point to add the shape
input: the index of the chosen shape in the vector
output: none
*/
void Menu::moveTheShape(const int chosenShape)
{
	double x = 0;
	double y = 0;

	system("CLS"); // clear the screen

	// get the point from the user
	std::cout << "Please enter the X moving scale: ";
	std::cin >> x;
	std::cout << "Please enter the Y moving scale: ";
	std::cin >> y;

	this->_shapes[chosenShape]->clearDraw(this->_canvas); // clear the screen
	this->_shapes[chosenShape]->move(Point(x, y)); // move the shape
	this->_shapes[chosenShape]->draw(this->_canvas); // draw it again
}

/*
function delete the whole screen and paint the shapes again 
called after removing a shape so the holes will be filled
input: none
output: none
*/
void Menu::afterChangeShape()
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->clearDraw(this->_canvas);
		this->_shapes[i]->draw(this->_canvas);
	}
}

/*
choice 3 and at the end of the program:
function delete all the sahpes 
input: the shapes vector
output: none
*/
void Menu::deleteAllShapes()
{
	int i = 0;
	for (i = this->_shapes.size() - 1; i >= 0; i--)
	{
		// popping each shape and clear it from the screen
		this->_shapes[i]->clearDraw(this->_canvas);
		delete this->_shapes[i];
		this->_shapes.pop_back();
	}
}

/*
bonus
function print the color menu and choose the right color according to his choice
input: none
output: the wanted color
*/
Color Menu::colorMenu() const
{
	int colorChoice = 0;
	Color color{};

	do {
		// menu
		std::cout << std::endl << "colors:" << std::endl << "--------" << std::endl;
		std::cout << "Enter 0 for red" << std::endl;
		std::cout << "Enter 1 for blue" << std::endl;
		std::cout << "Enter 2 for green" << std::endl;
		std::cout << "Enter 3 for white" << std::endl;
		std::cout << "Enter 4 for black" << std::endl;
		std::cin >> colorChoice;
		// checking valid
	} while (colorChoice < 0 || colorChoice > 4);

	// choose the coloe according to the choice
	switch (colorChoice)
	{
	case 0:
		color = Color::red;
		break;

	case 1:
		color = Color::blue;
		break;

	case 2:
		color = Color::green;
		break;

	case 3:
		color = Color::white;
		break;

	case 4:
		color = Color::black;
		break;
	}
	return color;
}