#pragma once
#include "Canvas.h"
#include "Menu.h"

#define NUM_OF_TYPES 4

class BetterCanvas
{
public:
	BetterCanvas();
	~BetterCanvas();
	void draw_shape(Shape* shape, Color color);

	std::vector<unsigned char> get_color(Color color) const;

private:
	Canvas _canvas;
};