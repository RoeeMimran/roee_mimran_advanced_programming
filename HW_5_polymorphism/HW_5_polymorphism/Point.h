#pragma once
#include <math.h>

class Point
{
private:
	//fields
	double _x;
	double _y;

public:
	//methods
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();

	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);

	double distance(const Point& other) const;

	// getters
	double getX() const;
	double getY() const;
};