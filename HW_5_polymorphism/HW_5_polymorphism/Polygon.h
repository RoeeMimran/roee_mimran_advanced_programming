#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:
	// constructors
	Polygon(const std::string& type, const std::string& name);
	virtual ~Polygon();

	// methods
	virtual double getArea() const = 0;
	virtual double getPerimeter() const = 0;
	virtual void draw(const Canvas& canvas) = 0;
	virtual void move(const Point& other); // add the Point to all the points of shape


protected:
	std::vector<Point> _points;
};

void addingPoints(std::vector<Point>& points, const Point& other);