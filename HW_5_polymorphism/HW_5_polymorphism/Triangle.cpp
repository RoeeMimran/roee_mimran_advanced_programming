#include "Triangle.h"
#include <iostream>

//c'tor
myShapes::Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

//d'tor
myShapes::Triangle::~Triangle()
{
	this->_points.pop_back();
	this->_points.pop_back();
	this->_points.pop_back();
}

double myShapes::Triangle::getArea() const
{
	// triangle formula with 3 points
	double part1 = this->_points[0].getX() * (this->_points[2].getY() - this->_points[1].getY());
	double part2 = this->_points[1].getX() * (this->_points[0].getY() - this->_points[2].getY());
	double part3 = this->_points[2].getX() * (this->_points[1].getY() - this->_points[0].getY());
	return abs(part1 + part2 + part3) / 2;
}

double myShapes::Triangle::getPerimeter() const
{
	// get every side of the trianlge and sum them
	double firstSide = this->_points[0].distance(this->_points[1]);
	double secondSide = this->_points[1].distance(this->_points[2]);
	double thirdSide = this->_points[0].distance(this->_points[2]);

	return firstSide + secondSide + thirdSide;
}

void myShapes::Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2], this->getColor());
}

void myShapes::Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

/*
function initialize triangle with his fields
input: none
output: pointer of dynamic triangle
*/
myShapes::Triangle* myShapes::Triangle::initializeTriangle()
{
	std::string name;
	double x[NUM_OF_POINTS] = { 0 };
	double y[NUM_OF_POINTS] = { 0 };
	int i = 0;
	Triangle* newTriangle = 0;
	bool valid = true;

	for (i = 0; i < NUM_OF_POINTS; i++)
	{
		std::cout << "Enter the X of point: " << i + 1 << std::endl;
		std::cin >> x[i];
		std::cout << "Enter the Y of point: " << i + 1 << std::endl;
		std::cin >> y[i];
	}
	std::cout << "Enter the name of the shape:" << std::endl;
	std::cin >> name;

	if (x[0] == x[1] && x[0] == x[2])
	{
		std::cout << "The points entered create a line." << std::endl;
		newTriangle = nullptr;
		system("PAUSE");
	}
	else if (y[0] == y[1] && y[0] == y[2])
	{
		std::cout << "The points entered create a line." << std::endl;
		newTriangle = nullptr;
		system("PAUSE");
	}
	else
	{
		newTriangle = new myShapes::Triangle(Point(x[0], y[0]), Point(x[1], y[1]), Point(x[2], y[2]), "triangle", name);
	}

	return newTriangle;
}
