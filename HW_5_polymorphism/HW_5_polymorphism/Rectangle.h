#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		// constructors
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name);
		virtual ~Rectangle();

		// getters
		virtual double getArea() const;
		virtual double getPerimeter() const;

		// methods
		virtual void draw(const Canvas& canvas);
		virtual void clearDraw(const Canvas& canvas);
		static Rectangle* initializeRectangle();
	};
}