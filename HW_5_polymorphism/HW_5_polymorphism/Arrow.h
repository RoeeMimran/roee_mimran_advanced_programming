#pragma once
#include "Polygon.h"

#define NUM_OF_POINTS 2

namespace myShapes
{
	class Arrow : public Shape
	{
	public:
		// constructors
		Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name);
		~Arrow();

		// getters and methods
		virtual double getArea() const;
		virtual double getPerimeter() const;
		virtual void draw(const Canvas& canvas);
		virtual void clearDraw(const Canvas& canvas);
		virtual void move(const Point& other); // add the Point to all the points of shape
		static Arrow* initializeArrow();

	private:
		std::vector<Point> _points;
	};
}