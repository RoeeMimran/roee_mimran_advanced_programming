#include "Polygon.h"
#include <string>

#define NUM_OF_POINTS 3

namespace myShapes
{
	class Triangle : public Polygon
	{
	public:
		// constructors
		Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
		virtual ~Triangle();

		// getters
		virtual double getArea() const;
		virtual double getPerimeter() const;

		// methods
		virtual void draw(const Canvas& canvas);
		virtual void clearDraw(const Canvas& canvas);

		static Triangle* initializeTriangle();
	};
}