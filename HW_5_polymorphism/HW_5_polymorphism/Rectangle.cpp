#include "Rectangle.h"
#include <iostream>

//c'tor
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type, name)
{
	Point temp(0, 0);

	// top left corner
	this->_points.push_back(a);

	//down right corner
	temp = a + Point(length, width);
	this->_points.push_back(temp);

	// top right corner
	temp = a + Point(length, 0);
	this->_points.push_back(temp);

	//down left corner
	temp = a + Point(0, width);
	this->_points.push_back(temp);
}

//d'tor
myShapes::Rectangle::~Rectangle()
{
	// poping all the points
	this->_points.pop_back();
	this->_points.pop_back();
	this->_points.pop_back();
	this->_points.pop_back();
}

double myShapes::Rectangle::getArea() const
{
	double width = this->_points[0].distance(this->_points[3]);
	double length = this->_points[0].distance(this->_points[2]);

	// area formula
	return width * length;
}

double myShapes::Rectangle::getPerimeter() const
{
	double width = this->_points[0].distance(this->_points[3]);
	double length = this->_points[0].distance(this->_points[2]);

	//perimeter perimeter
	return (width + length) * 2;
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1], this->getColor());
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

/*
function initialize rectangle with his fields
input: none
output: pointer of dynamic rectangle
*/
myShapes::Rectangle* myShapes::Rectangle::initializeRectangle()
{
	double x = 0;
	double y = 0;
	double length = 0;
	double width = 0;
	std::string name;
	myShapes::Rectangle* newRec = 0;

	std::cout << "Enter the x of the top left corner:" << std::endl;
	std::cin >> x;

	std::cout << "Enter the Y of the top left corner:" << std::endl;
	std::cin >> y;

	std::cout << "Please enter the length of the shape:" << std::endl;
	std::cin >> length;

	std::cout << "Please enter the width of the shape:" << std::endl;
	std::cin >> width;

	std::cout << "Enter the name of the shape:" << std::endl;
	std::cin >> name;

	// checking for valid
	if (length == 0 || width == 0)
	{
		std::cout << "Length or Width can't be 0." << std::endl;
		newRec = nullptr;
		system("PAUSE");
	}
	else
	{
		newRec = new myShapes::Rectangle(Point(x, y), length, width, "rectangle", name);
	}
	
	return newRec;
}
