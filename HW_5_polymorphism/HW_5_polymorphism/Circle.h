#include "Shape.h"
#include "Point.h"
#include "Polygon.h"

#define PI 3.14

namespace myShapes
{
	class Circle : public Shape
	{
	public:
		// constructors
		Circle(const Point& center, double radius, const std::string& type, const std::string& name);
		~Circle();

		// getters
		const Point& getCenter() const;
		double getRadius() const;

		// methods
		virtual double getArea() const;
		virtual double getPerimeter() const;
		virtual void move(const Point& other);

		// methods
		virtual void draw(const Canvas& canvas);
		virtual void clearDraw(const Canvas& canvas);

		static Circle* initializeCircle();

	private:
		double _radius;
		Point _center;
	};
	
	
}

