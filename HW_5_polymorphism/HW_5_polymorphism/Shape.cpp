#include "Shape.h"
#include <iostream>

//c'tor
Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

//d'tor
Shape::~Shape()
{
}

void Shape::printDetails() const
{
	std::cout << this->_type << " " << this->_name << "	" << this->getArea() << "  " << this->getPerimeter() << std::endl;
}

std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}

// color setter
void Shape::setColor(Color color)
{
	this->_color = color;
}

// color getter
Color Shape::getColor()
{
	return this->_color;
}
