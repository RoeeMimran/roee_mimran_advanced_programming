#include "Polygon.h"
#include <iostream>

Polygon::Polygon(const std::string& type, const std::string& name) : Shape(name, type)
{
}

Polygon::~Polygon()
{
}

/*
function move the shape according to the other poing
input: the point to add
output: none
*/
void Polygon::move(const Point& other)
{
	addingPoints(this->_points, other);
}

/*
function add the point to all other points in the shape
input: the vector with the points and the point to add each point in the vector
output: none
*/
void addingPoints(std::vector<Point>& points, const Point& other)
{
	int i = 0;
	for (i = 0; i < points.size(); i++)
	{
		points[i] += other;
	}
}



