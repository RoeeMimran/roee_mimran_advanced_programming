#pragma once
#include "Circle.h"

// c'tor
myShapes::Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type), _center(center)
{
	this->_radius = radius;
}

// d'tor
myShapes::Circle::~Circle()
{
}

const Point& myShapes::Circle::getCenter() const
{
	return this->_center;
}

double myShapes::Circle::getRadius() const
{
	return this->_radius;
}

double myShapes::Circle::getArea() const
{
	// area formula
	return PI * pow(this->_radius, 2);
}

double myShapes::Circle::getPerimeter() const
{
	// perimeter formula
	return 2 * PI * this->_radius;
}

void myShapes::Circle::move(const Point& other)
{
	this->_center += other;
}

void myShapes::Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius(), this->getColor());
}

void myShapes::Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

/*
function initialize circle with his fields
input: none
output: pointer of dynamic circle
*/
myShapes::Circle* myShapes::Circle::initializeCircle()
{
	double x = 0;
	double y = 0;
	double radius = 0;
	std::string name;
	myShapes::Circle* newCircle = 0;

	std::cout << "Please enter x:" << std::endl;
	std::cin >> x;

	std::cout << "Please enter y:" << std::endl;
	std::cin >> y;

	Point center(x, y);

	std::cout << "Please enter radius:" << std::endl;
	std::cin >> radius;

	std::cout << "Please enter the name of the shape" << std::endl;
	std::cin >> name;

	return new myShapes::Circle(center, radius, "circle", name);
}
