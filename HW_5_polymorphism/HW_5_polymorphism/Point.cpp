#include "Point.h"

//c'tor
Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

// copy constructor
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

//d'tor
Point::~Point()
{
}

Point Point::operator+(const Point& other) const
{
	double newX = this->_x + other._x;
	double newY = this->_y + other._y;
	Point newPoint(newX, newY);
	return newPoint;
}

Point& Point::operator+=(const Point& other)
{
	*this = *this + other;
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	// distance formula
	return sqrt(pow((this->_x - other._x), 2) + pow((this->_y - other._y), 2));
}
