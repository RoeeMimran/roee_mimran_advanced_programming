#pragma once

#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

#define MIN_N 2
#define EMPTY_VECTOR 0
#define EMPTY_VECTOR_ERROR -9999
#define NEGATIVE -1

class Vector
{
private:
	//Fields
	int* _elements;
	int _capacity; //Total memory allocated
	int _size; //Size of vector to access
	int _resizeFactor; // how many cells to add when need to reallocate

	//methods
	void changeArraySize(const int changeSize);
	void copyElements(const Vector& src);
	void copyVectors(const Vector& src);
	void negateVector();
	int	findLargeVector(const Vector& v1, const Vector& v2);

public:
	//A
	Vector(int n);
	~Vector();
	int size() const;//return size of vector
	int capacity() const;//return capacity of vector
	int resizeFactor() const; //return vector's resizeFactor
	bool empty() const; //returns true if size = 0
	int* getFirstElement() const; // return pointer of first element

	//B
	//Modifiers
	void push_back(const int& val);//adds element at the end of the vector
	int pop_back();//removes and returns the last element of the vector
	void reserve(int n);//change the capacity
	void resize(int n);//change _size to n, unless n is greater than the vector's capacity
	void assign(int val);//assigns val to all elemnts
	void resize(int n, const int& val);//same as above, if new elements added their value is val

	//C
	//The big three (d'tor is above)
	Vector(const Vector& other);
	Vector& operator=(const Vector& other);

	//D
	//Element Access
	int& operator[](int n) const;//n'th element

	//E
	//add and subtruct
	Vector operator+(const Vector& v1) const;
	Vector operator-(const Vector& v1) const;
	void operator+=(const Vector& v1);
	void operator-=(const Vector& v1);

	//printing
	friend std::ostream& operator<<(std::ostream& output, const Vector& v);
};

#endif