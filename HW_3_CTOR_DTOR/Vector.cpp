#include "Vector.h"
#include <iostream>

/*
C'TOR:
function create the vector ADT and initiallize his variables
input: the size of the vector
output: none
*/
Vector::Vector(int n)
{
	if (n < MIN_N)
	{
		n = MIN_N;
	}
	
	this->_size = 0;
	this->_capacity = n;
	this->_resizeFactor = n;
	
	this->_elements = new int[n];
}

/*
DTOR:
function free the memory of the vector
input: none
output: none
*/
Vector::~Vector()
{
	//set zero to all the variables
	while (this->_size > 0)
	{
		this->_size--;
		*(this->_elements + this->_size) = 0;
	}
	this->_capacity = 0;
	this->_resizeFactor = 0;	
	delete[] this->_elements; // free the dynamic memory of the array
}

/*
function change the size of the array
input: the new size of the array
output: none
*/
void Vector::changeArraySize(const int newSize)
{
	int i = 0;

	this->_capacity = newSize; // update the new capacity

	int* newArray = new int[this->_capacity];

	for (i = 0; i < this->_size; i++) // copy each number
	{
		*(newArray + i) = *(this->_elements + i);
	}

	delete[] this->_elements; // delete last array
	this->_elements = newArray;
}

/*
function add a new number to the vector
input: the new value
output: none
*/
void Vector::push_back(const int& val)
{
	if (this->_size >= this->_capacity) // checking if the vector is full
	{
		this->changeArraySize(this->_resizeFactor + this->_capacity);
	}
	
	*(this->_elements + this->_size) = val; // add the new value
	this->_size++;
}

/*
function remove the top of the vector
input: none
output: the value of the first or -9999 if empty
*/
int Vector::pop_back()
{
	int return_value = 0;

	if (this->empty()) // checking if the vector is empty
	{
		return_value = EMPTY_VECTOR_ERROR;
	}
	else //save the first value and remove it from the vector
	{
		this->_size--;
		int* lastElement = this->_elements + this->_size;
		return_value = *lastElement;
		*lastElement = 0;
	}
	return return_value;
}

/*
function change the capacity of the vector if n bigger than capacity
input: n - the number that capacity should be bigger
output: none
*/
void Vector::reserve(int n)
{
	while (this->_capacity < n)
	{
		this->changeArraySize(this->_resizeFactor + this->_capacity);
	}
}

/*
function resize the vector 
input: n - the new size of the vector
output: none
*/
void Vector::resize(int n)
{
	if (n < this->_capacity)
	{
		this->_size = n; // update the new size
		this->changeArraySize(n);
	}
	else if(n > this->_capacity)
	{
		this->reserve(n);
	}
}

/*
function change all the numbers in vector to the val
input: a numebr to change all
output: none
*/
void Vector::assign(int val)
{
	int i = 0;

	for (i = 0; i < this->_size; i++)
	{
		*(this->_elements + i) = val;
	}
}

/*
function resize the vector and cahnge all the numbers to the val
input: n- the new size of the vector, val - the new value to all the numbers
output: none
*/
void Vector::resize(int n, const int& val)
{
	this->resize(n);
	this->assign(val);
}

/*
function copy each variable in the vectors
input: src - the src of the variables
output: none
*/
void Vector::copyVectors(const Vector& src)
{
	this->_capacity = src._capacity;
	this->_resizeFactor = src._resizeFactor;
	this->copyElements(src);
}

/*
function copy each number in the array to the new vector
input: src - the source array to copy from
output: none
*/
void Vector::copyElements(const Vector& src)
{
	this->_size = 0;
	this->_elements = new int[this->_capacity];

	while (this->_size < src._size)
	{
		*(this->_elements + this->_size) = *(src._elements + this->_size);
		this->_size++;
	}
}

/*
copy constructor:
function copy the vector
input: other - the vector to copy from
output: none
*/
Vector::Vector(const Vector& other)
{
	this->copyVectors(other);
}

/*
copy constructor:
function copy the vector with the operator =
input: other - the vector to copy from
output: the new vector
*/
Vector& Vector::operator=(const Vector& other)
{
	delete[] this->_elements;
	this->copyVectors(other);
	return *this;
}

/*
function return the n value in the vector
input: n - the index in the vector
output: reference of the value
*/
int& Vector::operator[](int n) const
{
	int* elementP = 0;

	if (n + 1 > this->_size || n < 0) // checking if n is exceeded from the size
	{
		std::cout << "the index is exceeded from the size of the vector" << std::endl;
		elementP = this->_elements;
	}
	else
	{
		elementP = this->_elements + n;
	}

	return *elementP;
}

/*
function find the large vector and put it in v3
input: v1, v2 - the vecotrs to compare
output: the size of the small vector
*/
int Vector::findLargeVector(const Vector& v1, const Vector& v2)
{
	int smallVectorSize = 0;

	if (v1._size > v2._size)
	{
		*this = v1;
		smallVectorSize = v2._size;
	}
	else
	{
		*this = v2;
		smallVectorSize = v1._size;
	}
	return smallVectorSize;
}

/*
function is summing 2 vectors
input: the first vector and this as the second vector
output: the sum of the vectors
*/
Vector Vector::operator+(const Vector& v1) const
{
	int i = 0;
	int smallVectorSize = 0;
	int bigVectorSize = 0;
	Vector v3(0);
	
	smallVectorSize = v3.findLargeVector(*this, v1);		

	for (i = 0; i < smallVectorSize; i++) // sum the vectors and save in the new vector
	{
		*(v3._elements + i) = *(this->_elements + i) + *(v1._elements + i);
	}

	return v3;
}

/*
function make all the numbers in the vectors negative
input: none
output: none
*/
void Vector::negateVector()
{
	int i = 0;

	for (i = 0; i < this->_size; i++) // pass on each num
	{
		*(this->_elements + i) = NEGATIVE * *(this->_elements + i);
	}
}

/*
function subtruct 2 vectors 
input: v2, this - the vectors to subtruct
output: v3 - the vector after the subtruct
*/
Vector Vector::operator-(const Vector& v2) const
{
	Vector temp(v2);
	temp.negateVector(); // negative all the number in the vectors

	Vector v3 = *this + temp; // summin the vectors
	return v3;
}

/*
function add two vectors and put it in the left
input: the vector to add
output: none
*/
void Vector::operator+=(const Vector& v1)
{
	*this = *this + v1;
}

/*
function subtruct two vectors and put it in the left
input: the vector to add
output: none
*/
void Vector::operator-=(const Vector& v1)
{
	*this = *this - v1;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

/*
function check if the vector empty
input: none
output: empty or not
*/
bool Vector::empty() const
{
	return this->_size == EMPTY_VECTOR;
}

int* Vector::getFirstElement() const
{
	return this->_elements;
}

/*
function print the vector
input: the vector variables and theoutput that will be printed
output: the output that will be printed
*/
std::ostream& operator<<(std::ostream& output, const Vector& v)
{
	int i = 0;

	output << "Vector Info : \nCapacity is{ " << v._capacity << " }\nSize is{ " << v._size << " }\ndata is{ ";
	
	for (i = 0; i < v._size - 1; i++) // print the numebrs until one before the last one
	{
		output << *(v._elements + i) << ", "; // so the coma wont be on the last too
	}
	output << *(v._elements + i); // print the last one
	
	output << " }\n";

	return output;
}
