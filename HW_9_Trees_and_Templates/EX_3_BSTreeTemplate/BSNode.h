#pragma once

#include <string>
#include <iostream>

#define NOT_FOUND -1

template <class type>
class BSNode
{
public:
	BSNode(type data);
	BSNode(const BSNode& other);

	~BSNode();

	void insert(type value);
	BSNode<type>& operator=(const BSNode<type>& other);

	bool isLeaf() const;
	type getData() const;
	BSNode<type>* getLeft() const;
	BSNode<type>* getRight() const;

	bool search(type val) const;

	int getHeight() const;
	int getDepth(const BSNode<type>& root) const;

	void printNodes() const; //for question 1 part C

private:
	type _data;
	BSNode<type>* _left;
	BSNode<type>* _right;
				
	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode<type>* node) const; //auxiliary function for getDepth

};

int findMax(int a, int b);

/*
c'tor
create a node of the tree
input: the wanted data
output: none
*/
template<class type>
BSNode<type>::BSNode(type data)
{
	this->_data = data;
	this->_left = 0;
	this->_right = 0;
	this->_count = 1;
}

/*
copy constructor
function copy a full tree
input: other tree
output: none
*/
template <class type>
BSNode<type>::BSNode(const BSNode<type>& other)
{
	*this = other; // copy the current node

	// check if there is left
	if (this->_left)
	{
		// send the left node to the same function - recurisve
		this->_left = new BSNode<type>(*this->_left);
	}

	// check if there is right
	if (this->_right)
	{
		// send the right node to the same function - recurisve
		this->_right = new BSNode<type>(*this->_right);
	}
}

/*
d'tor
function delete all the located memory
input: none
output: none
*/
template <class type>
BSNode<type>::~BSNode()
{
	if (this->_left)
	{
		// delete the left so we will send the left to the d'tor
		delete this->_left;
	}

	if (this->_right)
	{
		// the same for the right
		delete this->_right;
	}
}

/*
function insert a new node to the tree
input: the new value
output: none
*/
template<class type>
void BSNode<type>::insert(type value)
{
	// get the next one
	BSNode<type>** next = &this->_left;

	// check if the data should be left or right
	if (this->_data < value)
	{
		next = &this->_right;
	}

	// checking if the value is the same
	if (this->_data != value)
	{
		// checking if there is a next
		if (*next)
		{
			(*next)->insert(value);
		}
		else
		{
			// create the new node
			*next = new BSNode(value);
		}
	}
	else
	{
		this->_count++;
	}
}

/*
function copy the node
input: the node to copy
output: reference of the new node
*/
template <class type>
BSNode<type>& BSNode<type>::operator=(const BSNode<type>& other)
{
	this->_data = other._data;
	this->_left = 0;
	this->_right = 0;
	return *this;
}

/*
function check if the current node is a leaf
input: none
output: leaf or not
*/
template <class type>
bool BSNode<type>::isLeaf() const
{
	return this->_left || this->_right;
}

template <class type>
type BSNode<type>::getData() const
{
	return this->_data;
}

template <class type>
BSNode<type>* BSNode<type>::getLeft() const
{
	return this->_left;
}

template <class type>
BSNode<type>* BSNode<type>::getRight() const
{
	return this->_right;
}

/*
function check if the value exist in the tree
input: the value
output: exist or none
*/
template<class type>
bool BSNode<type>::search(type val) const
{
	bool found = false;

	if (this->_data == val)
	{
		found = true;
	}
	else
	{
		if (this->_left)
		{
			found = this->_left->search(val);
		}

		// check if we found it in the left path
		if (this->_right && !found)
		{
			found = this->_right->search(val);
		}
	}

	return found;
}

/*
function get the biggest path to the last node
input: none
output: the number of nodes in the way
*/
template <class type>
int BSNode<type>::getHeight() const
{
	int leftPath = 0;
	int rightPath = 0;
	int maxHeight = 0;

	// checking if there is left
	if (this->_left)
	{
		// send the left recursively
		leftPath = this->_left->getHeight();
	}

	// checking if there is right
	if (this->_right)
	{
		// send the right recursively
		rightPath = this->_right->getHeight();
	}


	// checking what bigger
	maxHeight = findMax(rightPath, leftPath);

	// and return it plus 1 because the current node
	return 1 + maxHeight;
}

/*
funciton check iff the value exist and
get the distance bewteen current node and the input node
input: the root to get distance from
output: the number of nodes in the path or -1 if not found
*/
template <class type>
int BSNode<type>::getDepth(const BSNode<type>& root) const
{
	// checking if the value exist in this path
	if (root.search(this->_data))
	{
		return this->getCurrNodeDistFromInputNode(&root);
	}
	else
	{
		return NOT_FOUND;
	}
}

/*
function print the nodes according to the abc and the count
input: none
output: none
*/
template <class type>
void BSNode<type>::printNodes() const
{
	if (this->_left)
	{
		this->_left->printNodes();
	}

	// cheking if there is not left print the current
	std::cout << this->_data << " " << this->_count << std::endl;

	if (this->_right)
	{
		this->_right->printNodes();
	}
}

/*
function get the dist between two nodes
input: one of the nodes
output: the number of nodes in the path
*/
template <class type>
int BSNode<type>::getCurrNodeDistFromInputNode(const BSNode<type>* node) const
{
	int leftPath = 0;
	int rightPath = 0;
	int maxHeight = 0;

	// checking if the current node is the node we are looking for
	if (this->_data != node->_data)
	{
		// send the left path
		if (node->_left)
		{
			leftPath = this->getCurrNodeDistFromInputNode(node->_left);
		}

		// send the right path
		if (node->_right)
		{
			rightPath = this->getCurrNodeDistFromInputNode(node->_right);
		}
	}

	// find the larger path
	maxHeight = findMax(rightPath, leftPath);

	// and add 1 because the current node
	return 1 + maxHeight;
}

/*
function find the bigger number
input: two numbers to compare
output: the bigger number
*/
int findMax(int a, int b)
{
	if (a > b)
	{
		return a;
	}
	else
	{
		return b;
	}
}