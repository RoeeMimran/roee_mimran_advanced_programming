#include "BSNode.h"

#define SIZE_OF_ARRAY 15

/*
function the array 
input: array and the size of the array
output: none
*/
template <class type>
void printArr(type* arr, unsigned int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}

/*
function sort the array using the template tree and print it
input: the array and his size
output: none
*/
template <class type>
void sort(type* arr, unsigned int size)
{
	// create the head of the tree
	BSNode<type>* bs = new BSNode<type>(arr[0]);

	// create all the other tree
	for (int i = 1; i < size; i++)
	{
		bs->insert(arr[i]);
	}

	// print the tree
	bs->printNodes();
	std::cout << std::endl;
	delete bs;
}

int main()
{
	int intArr[SIZE_OF_ARRAY] = { 2, 3, 12, 9, 6, 7, 8, 5, 10, 13, 4, 11, 14, 15, 1 };
	std::string stringArr[SIZE_OF_ARRAY] = { "d", "c", "a", "b", "e", "g", "h", "j", "a", "b", "c", "o", "p", "t", "y" };

	// print the arrays
	std::cout << "int: ";
	printArr(intArr, SIZE_OF_ARRAY);

	std::cout << "string: ";
	printArr(stringArr, SIZE_OF_ARRAY);
	std::cout << std::endl;
	std::cout << std::endl;

	// sort them
	std::cout << "sorted int array:" << std::endl;
	sort(intArr, SIZE_OF_ARRAY);
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "sorted string array:" << std::endl;
	sort(stringArr, SIZE_OF_ARRAY);

	system("PAUSE");
	return 0;
}