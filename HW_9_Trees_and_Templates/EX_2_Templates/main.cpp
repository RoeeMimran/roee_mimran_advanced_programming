#include "functions.h"
#include <iostream>
#include "Class.h"


int main() {

	// double
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;


	// char
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a' ,'c') << std::endl;
	std::cout << compare<char>('e', 'b') << std::endl;
	std::cout << compare<char>('j', 'j') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;
	
	const int arr_sizeC = 5;
	char charArr[arr_sizeC] = { 'a', 'd', 'g', 'e', 'b' };
	bubbleSort<char>(charArr, arr_sizeC);
	for (int i = 0; i < arr_sizeC; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_sizeC);
	std::cout << std::endl;
	

	// class
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<Class>(Class(2), Class(3)) << std::endl;
	std::cout << compare<Class>(Class(5), Class(3)) << std::endl;
	std::cout << compare<Class>(Class(4), Class(4)) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size_class = 5;
	Class classArr[arr_size_class] = { Class(10), Class(3), Class(5), Class(6), Class(1) };
	bubbleSort<Class>(classArr, arr_size_class);
	for (int i = 0; i < arr_sizeC; i++) {
		std::cout << classArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<Class>(classArr, arr_size_class);
	std::cout << std::endl;

	system("pause");
	return 1;
}