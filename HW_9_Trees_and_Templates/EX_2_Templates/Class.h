#pragma once

#include <iostream>

class Class
{
public:
	int _member;
	
	// c'tor
	Class(int member)
	{
		this->_member = member;
	}

	bool operator<(Class& other)
	{
		if (this->_member < other._member)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool operator>(const Class& other)
	{
		if (this->_member > other._member)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void operator=(const Class& other)
	{
		this->_member = other._member;
	}

	friend std::ostream& operator<<(std::ostream& out, const Class& c)
	{
		out << c._member;
		return out;
	}
};