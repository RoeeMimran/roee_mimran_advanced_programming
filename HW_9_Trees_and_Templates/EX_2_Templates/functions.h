#pragma once

#define FIRST_BIGGER -1
#define SECOND_BIGGER 1
#define EQUAL 0

/*
function compare 2 types of numbers and return the bigger
input: two variables
output: -1 if first bigger, 0 if equal and 1 if second bigger
*/
template <class type>
int compare(type a, type b)
{
	if (a > b)
	{
		return FIRST_BIGGER;
	}
	else if (b > a)
	{
		return SECOND_BIGGER;
	}
	else
	{
		return EQUAL;
	}
}

/*
function sort an array, bubble sort
input: array of templates, and the size of the array
output: none
*/
template <class type>
void bubbleSort(type* arr, unsigned int size)
{
	int i = 0;
	int j = 0;

	// bubble sort
	for (i = 0; i < size; i++)
	{
		for (j = i + 1; j < size; j++)
		{
			// compare the 2 numbers
			if (compare(arr[i], arr[j]) == FIRST_BIGGER)
			{
				// swapping them
				type temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
}

/*
function print the array
input: the array and the size of the array
output: none
*/
template <class type>
void printArray(type* arr, unsigned int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		std::cout << arr[i] << " ";
	}
}