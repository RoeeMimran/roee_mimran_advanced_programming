#pragma once
#include <exception>
class MultiplySelectionExeption : public std::exception
{
	virtual const char* what() const
	{
		// clear the error flags
		std::cin.clear();

		// clear the inputs that waiting in the stream
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		return "Warning - Don't try to build more than one shape at once\n";
	}
};