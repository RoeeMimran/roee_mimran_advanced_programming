#pragma once
#include "shape.h"
#include "MathUtils.h"

class Pentagon : public Shape
{
private:
	double _side;

public:
	// c'tor
	Pentagon(std::string name, std::string color, double side);

	// setter
	void setSide(double side);

	// getter
	double getSide(void);

	void draw(void);
};