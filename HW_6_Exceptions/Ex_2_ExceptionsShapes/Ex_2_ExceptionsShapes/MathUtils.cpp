#include "MathUtils.h"

/*
funciton calculate pentagon area from his side
input: the side of the pentagon
output: his area
*/
double MathUtlis::calPentagonArea(double side)
{
	// pentagon area formula
	double area = 0.25 * sqrt(5 * (5 + 2 * sqrt(5))) * pow(side, 2);
	return area;
}

/*
funciton calculate hexagon area from his side
input: the side of the hexagon
output: his area
*/
double MathUtlis::calHexagonArea(double side)
{
	// hexagon formula area
	double area = (3 * sqrt(3) / 2) * pow(side, 2);
	return area;
}
