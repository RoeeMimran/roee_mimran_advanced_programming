#pragma once
#include <exception>
#include <iostream>
class InputException : public std::exception
{
	virtual const char* what() const
	{
		// clear the error flags
		std::cin.clear();

		// clear the inputs that waiting in the stream
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		
		return "you enterd letter and you need to enter number\n";
	}
};

