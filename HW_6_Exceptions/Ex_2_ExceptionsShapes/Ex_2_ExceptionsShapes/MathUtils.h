#pragma once
#include "shape.h"
class MathUtlis : public Shape
{
public:
	static double calPentagonArea(double side);
	static double calHexagonArea(double side);
};