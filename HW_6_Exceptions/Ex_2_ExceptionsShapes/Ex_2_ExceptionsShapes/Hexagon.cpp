#include "Hexagon.h"
#include "shapeexception.h"

// c'tor
Hexagon::Hexagon(std::string name, std::string color, double side) : Shape(name, color)
{
	this->_side = side;
}

// setter
void Hexagon::setSide(double side)
{
	if (side < 0)
	{
		// checking if side is negative
		throw shapeException();
	}
	this->_side = side;
}

// getter
double Hexagon::getSide(void)
{

	return this->_side;
}

void Hexagon::draw(void)
{
	std::cout << std::endl << "Name is " << this->getName() << std::endl <<
		"Color is " << this->getColor() << std::endl << "side is: " << this->getSide()
		<< std::endl << "area: " << MathUtlis::calHexagonArea(getSide()) << std::endl;
}
