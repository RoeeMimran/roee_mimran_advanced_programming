#include <iostream>
#include "Hexagon.h"
#include "Pentagon.h"
#include "MultiplySelectionExeption.h"
#include "InputException.h"
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon penta(nam, col, width);
	Hexagon hexa(nam, col, width);

	Shape* ptrcirc = &circ;
	Shape* ptrquad = &quad;
	Shape* ptrrec = &rec;
	Shape* ptrpara = &para;
	Shape* ptrpenta = &penta;
	Shape* ptrhexa = &hexa;

	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', pentagon = 't', hexagon = 'h'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, t = pentagon, h = hexagon" << std::endl;
		std::cin >> shapetype;

		try
		{
			if (getchar() != '\n')
			{
				// when the user enter a multiply choice	
				throw MultiplySelectionExeption();
			}
			
			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;

				if (!(std::cin >> col >> nam >> rad))
				{
					throw InputException();
				}
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				
				if (!(std::cin >> nam >> col >> height >> width))
				{
					throw InputException();
				}
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;

				if (!(std::cin >> nam >> col >> height >> width))
				{
					throw InputException();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;

				if (!(std::cin >> nam >> col >> height >> width >> ang >> ang2))
				{
					throw InputException();
				}

				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			
			case 't':
				std::cout << "enter name, color, 1 side" << std::endl;
				if (!(std::cin >> nam >> col >> width))
				{
					throw InputException();
				}

				penta.setName(nam);
				penta.setColor(col);
				penta.setSide(width);
				penta.draw();
				break;

			case 'h':
				std::cout << "enter name, color, 1 side" << std::endl;
				if (!(std::cin >> nam >> col >> width))
				{
					throw InputException();
				}

				hexa.setName(nam);
				hexa.setColor(col);
				hexa.setSide(width);
				hexa.draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (std::exception& e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("pause");
	return 0;

}