#include "Pentagon.h"
#include "shapeexception.h"

// c'tor
Pentagon::Pentagon(std::string name, std::string color, double side) : Shape(name, color)
{
	this->setSide(side);
}

// setter
void Pentagon::setSide(double side)
{
	if (side < 0)
	{
		// checking if side is negative
		throw shapeException();
	}
	this->_side = side;
}

// getter
double Pentagon::getSide(void)
{
	return this->_side;
}

void Pentagon::draw(void)
{
	std::cout << std::endl << "Name is " << this->getName() << std::endl <<
		"Color is " << this->getColor() << std::endl << "side is: " << this->getSide()
		<< std::endl << "area: " << MathUtlis::calPentagonArea(getSide()) << std::endl;
}
