#include <iostream>

#define RETURN_ERROR_VALUE -1
#define ERROR_VALUE 8200
#define ERROR_STRING "This user is not authorized to access " << ERROR_VALUE << ", please enter different numbers, or try to get clearance in 1 year."

/*
function print the error string
input: none
output: none
*/
void printError()
{
	std::cout << ERROR_STRING << std::endl;
}

/*
function check if the number is the error value
input: the number to check
output: equal to error number or not
*/
bool checkError(int num)
{
	bool valid = true;
	if (num == ERROR_VALUE || num == RETURN_ERROR_VALUE)
	{
		valid = false;
	}
	return valid;
}

/*
function check the input, get two parameters the 2 inputs
and throw a exeption and print the error
input: the 2 input from the function
output: none
*/
void checkInput(int a, int b)
{
	if (!checkError(a) || !checkError(b))
	{
		printError();
		throw ERROR_VALUE;
	}
}

/*
function check the output at the end of the function
and print the error and throw an exeption
input: the output 
output: none
*/
void checkOutput(int output)
{
	if (!checkError(output))
	{
		printError();
		throw ERROR_VALUE;
	}
}

int add(int a, int b) {
	int sum = 0;
	try
	{
		// checking if there is error with input or output
		checkInput(a, b);
		sum = a + b;
		checkOutput(sum);
	}
	catch (const int)
	{
		sum = RETURN_ERROR_VALUE;
	}
	return sum;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b && sum != RETURN_ERROR_VALUE; i++) {
		sum = add(sum, a);
	}
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b && exponent != RETURN_ERROR_VALUE; i++) {
		exponent = multiply(exponent, a);
	};
	
	return exponent;
}

int main(void) {
	std::cout << pow(8200, 5) << std::endl;
	std::cout << pow(4, 5) << std::endl;
	std::cout << multiply(2050, 5) << std::endl;
	std::cout << add(8000, 200) << std::endl;
	system("PAUSE");
}