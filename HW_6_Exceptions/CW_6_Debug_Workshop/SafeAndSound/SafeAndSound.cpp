#include "part1.h"
#include "part2.h"
#include <iostream>

int main()
{
	part1();

	part2();	// debug this after the completion of part1
	system("PAUSE");
	return 0;
}
