#include <iostream>

struct Password
{
	char value[16];
	bool incorrect;
	Password() : value(""), incorrect(true)
	{
	}
};

int main()
{
	std::cout << "Enter your password to continue:" << std::endl;
	Password pwd;
	std::cin >> pwd.value;

	if (!strcmp(pwd.value, "********"))
		pwd.incorrect = false;

	if (!pwd.incorrect)
		// every pwd that his size is 16, replace the 0 in the string
		std::cout << "Congratulations\n";
	system("PAUSE");
	return 0;
}
