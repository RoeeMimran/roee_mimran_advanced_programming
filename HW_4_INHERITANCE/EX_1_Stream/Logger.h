#pragma once

class Logger
{
public:
	Logger(const char* filename, bool logToScreen);
	~Logger();

	void print(const char* msg);
};
