#pragma once
#include "FileStream.h"

class Logger
{
private:
	OutStream _screen;
	FileStream _file;
	bool _logToScreen;
	unsigned static int counter;

public:
	Logger(const char* filename, bool logToScreen);
	~Logger();

	void print(const char* msg);
};
