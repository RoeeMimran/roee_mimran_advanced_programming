#define _CRT_SECURE_NO_WARNINGS
#include "FileStream.h"

FileStream::FileStream(const char* filePath) : OutStream()
{
	this->_file = fopen(filePath, "w");
}

FileStream::~FileStream()
{
	fclose(this->_file);
}
