#include "Logger.h"
unsigned int Logger::counter = 1;

Logger::Logger(const char* filename, bool logToScreen) : _file(filename)
{
	this->_logToScreen = logToScreen;
}

Logger::~Logger()
{
}

void Logger::print(const char* msg)
{
	this->_file << "counter: " << this->counter << "\n";
	this->_screen << "counter: " << this->counter << "\n";

	this->_file << msg;

	if (this->_logToScreen)
	{
		this->_screen << msg;
	}
	
	this->counter++;
}
