#include "Logger.h"

Logger::Logger(const char* filename, bool logToScreen) : _file(filename)
{
	this->_logToScreen = logToScreen;
}

Logger::~Logger()
{
}

void Logger::print(const char* msg)
{
	this->_file << msg;

	if (this->_logToScreen)
	{
		this->_screen << msg;
	}
}
