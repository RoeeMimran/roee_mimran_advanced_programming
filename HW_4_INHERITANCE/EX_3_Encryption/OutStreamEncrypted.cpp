#include "OutStreamEncrypted.h"

OutStreamEncrypted::OutStreamEncrypted(const int encryptNum) : OutStream()
{
	_encryptNum = encryptNum;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char* str)
{
	OutStream out;

	char* encryptedString = encrpyt(str, this->_encryptNum);
	out << encryptedString;

	delete[] encryptedString;
	return *this;
}

/*
function encrypt the wanted string with the encrypt number
input: the srting and the encrypt num to add each letter
output: the encryped string
*/
char* encrpyt(const char* string, const int encrpytNum)
{
	int i = 0;
	int asciiValue = 0;
	int stringLen = countStringSize(string);

	char* encryptedString = new char[stringLen];

	for (i = 0; string[i]; i++)
	{
		asciiValue = string[i] + encrpytNum;
		
		checkAscii(&asciiValue); // cycle the ascii value so he will be in the limits

		encryptedString[i] = (char)asciiValue; // upadate the encrypted string
	}

	encryptedString[stringLen - 1] = 0; // set last letter 0, end of string
	return encryptedString;
}

/*
function check if the ascii value in the limits
input: pointer of the asciivalue so we will be able to change him
output: none
*/
void checkAscii(int* asciiValue)
{
	while (*asciiValue > MAX_ASCII) // max limit
	{
		*asciiValue = *asciiValue + MIN_ASCII - MAX_ASCII; // cycle the ascii
	}

	while (*asciiValue < MIN_ASCII) // min limit
	{
		*asciiValue = *asciiValue - MIN_ASCII + MAX_ASCII;
	}
}

/*
function count the length of the string
input: a string
output: the size of the stirng
*/
int countStringSize(const char* string)
{
	int i = 0;
	for (i = 0; string[i]; i++)
	{
	}
	i++; // save place for 0 at the end of the string
	return i;
}
