#pragma once
#include "OutStream.h"

#define MIN_ASCII 32
#define MAX_ASCII 126
class OutStreamEncrypted : public OutStream
{
private:
	int _encryptNum;
public:
	OutStreamEncrypted(const int encryptNum);
	OutStreamEncrypted& operator<<(const char* str);
};

char* encrpyt(const char* string, const int encrpytNum);
int countStringSize(const char* string);
void checkAscii(int* letter);
#pragma once
